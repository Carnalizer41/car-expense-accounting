package com.shyrkov.service;

import com.shyrkov.persistence.CarDao;
import com.shyrkov.persistence.CurrencyDao;
import com.shyrkov.persistence.RepairedDetailsDao;
import com.shyrkov.persistence.UserDao;
import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.persistence.entities.RepairedDetailsEntity;
import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.service.mappers.RepairedDetailInfoMapper;
import com.shyrkov.service.mappers.RepairedDetailsMapper;
import com.shyrkov.service.mappers.SpendingMapper;
import com.shyrkov.service.model.MoneyByCurrency;
import com.shyrkov.service.model.RepairedDetailStatistics;
import com.shyrkov.service.model.UserModel;
import com.shyrkov.service.validation.CarIdValidator;
import com.shyrkov.service.validation.NewRepairedDetailValidator;
import com.shyrkov.service.validation.YearValidator;
import com.shyrkov.web.dto.*;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class RepairedDetailsService {

    private final RepairedDetailsDao repairedDetailsDao;
    private final UserDao userDao;
    private final CurrencyDao currencyDao;
    private final CarDao carDao;
    private final RepairedDetailsMapper repairedDetailsMapper;
    private final SpendingMapper spendingMapper;
    private final RepairedDetailInfoMapper detailInfoMapper;
    private final SpendingService spendingService;
    private final DateFormer dateFormer;
    private final CurrencyConverter currencyConverter;
    private final NewRepairedDetailValidator repairedDetailValidator;
    private final CarIdValidator carIdValidator;
    private final YearValidator yearValidator;

    public RepairedDetailsService(RepairedDetailsDao repairedDetailsDao,
            UserDao userDao, CurrencyDao currencyDao, CarDao carDao,
            RepairedDetailsMapper repairedDetailsMapper,
            SpendingMapper spendingMapper,
            RepairedDetailInfoMapper detailInfoMapper,
            SpendingService spendingService, DateFormer dateFormer,
            NewRepairedDetailValidator repairedDetailValidator,
            CurrencyConverter currencyConverter, CarIdValidator carIdValidator,
            YearValidator yearValidator) {
        this.repairedDetailsDao = repairedDetailsDao;
        this.userDao = userDao;
        this.currencyDao = currencyDao;
        this.carDao = carDao;
        this.repairedDetailsMapper = repairedDetailsMapper;
        this.spendingMapper = spendingMapper;
        this.detailInfoMapper = detailInfoMapper;
        this.spendingService = spendingService;
        this.dateFormer = dateFormer;
        this.repairedDetailValidator = repairedDetailValidator;
        this.currencyConverter = currencyConverter;
        this.carIdValidator = carIdValidator;
        this.yearValidator = yearValidator;
    }

    public RepairedDetailDto addNewDetail(RepairedDetailDto repairedDetailDto) {
        repairedDetailValidator.validate(repairedDetailDto);

        SpendingDto spendingDto = spendingService.addNewSpending(
                repairedDetailDto.getSpendingDto());
        repairedDetailDto.setSpendingDto(spendingDto);

        RepairedDetailsEntity detailsEntity = repairedDetailsMapper.dtoToEntity(
                repairedDetailDto);
        detailsEntity.setDetailsSpendingEntity(
                spendingMapper.dtoToEntity(spendingDto));

        RepairedDetailDto savedDto = repairedDetailsMapper.entityToDto(
                repairedDetailsDao.save(detailsEntity));
        savedDto.setSpendingDto(spendingDto);

        return savedDto;
    }

    @PreAuthorize("hasRole('ADMIN')")
    public List<RepairedStatisticsDto> getRepairedDetailsStatistics(
            String mark) {

        List<RepairedStatisticsDto> repairedStatisticsDtos = new ArrayList<>();
        for (RepairedDetailStatistics detailStatistics : repairedDetailsDao.getDetailsStatistics(
                mark)) {
            repairedStatisticsDtos.add(
                    new RepairedStatisticsDto(detailStatistics.getName(),
                            String.format("%.2f",
                                    (double) detailStatistics.getAmount()
                                            / carDao.getAmountByMark(mark)),
                            String.format("%.2f",
                                    (double) detailStatistics.getCost()
                                    / detailStatistics.getAmount())));
        }

        return repairedStatisticsDtos;
    }

    public MoneySpentDto getYearMoneySpentByCar(Long carId, Integer year,
            String desiredCurrency) {
        carIdValidator.validate(carId);
        yearValidator.validate(year);

        MoneySpentDto spentDto = new MoneySpentDto();

        Date startDate = dateFormer.getDateByYear(year);
        Date endDate = dateFormer.getDateByYear(year + 1);

        List<MoneyByCurrency> moneySpentByCar = repairedDetailsDao.getMoneySpentByCar(
                carId, dateFormer.toString(startDate),
                dateFormer.toString(endDate));

        CurrencyEntity currencyEntity = currencyDao.findOneByCode(
                desiredCurrency);
        spentDto.setMoneySpent(
                currencyConverter.convert(moneySpentByCar, currencyEntity));

        return spentDto;
    }

    public MoneySpentDto getYearMoneySpentByUser(Integer year,
            String desiredCurrency) {
        yearValidator.validate(year);

        MoneySpentDto spentDto = new MoneySpentDto();

        Date startDate = dateFormer.getDateByYear(year);
        Date endDate = dateFormer.getDateByYear(year + 1);

        UserModel principal = (UserModel) SecurityContextHolder.getContext()
                .getAuthentication().getPrincipal();
        UserEntity user = userDao.findOneByEmail(principal.getEmail());

        List<MoneyByCurrency> moneySpentByUser = repairedDetailsDao.getMoneySpentByUser(
                user.getId(), dateFormer.toString(startDate),
                dateFormer.toString(endDate));

        CurrencyEntity currencyEntity = currencyDao.findOneByCode(
                desiredCurrency);
        spentDto.setMoneySpent(
                currencyConverter.convert(moneySpentByUser, currencyEntity));

        return spentDto;
    }

    public List<RepairedDetailInfoDto> getRepairedDetailsListForCar(
            Long carId) {
        carIdValidator.validate(carId);

        List<RepairedDetailInfoDto> detailInfoDtos = new ArrayList<>();

        for (RepairedDetailsEntity detailsEntity : repairedDetailsDao.findByCarId(
                carId)) {
            detailInfoDtos.add(detailInfoMapper.entityToDto(detailsEntity));
        }

        return detailInfoDtos;
    }
}
