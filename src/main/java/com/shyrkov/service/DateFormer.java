package com.shyrkov.service;

import com.shyrkov.exceptions.DateFormerException;
import org.springframework.stereotype.Component;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.format.DateTimeParseException;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Component
public class DateFormer {

    private final SimpleDateFormat sdf;

    public DateFormer() {
        String pattern = "yyyy-MM-dd";
        this.sdf = new SimpleDateFormat(pattern);
    }

    public Date getDateByYear(int year) {
        Calendar cal = new GregorianCalendar(year, Calendar.JANUARY, 1);

        return cal.getTime();
    }

    public Date getDateByMonthAndYear(int month, int year) {
        Calendar cal = new GregorianCalendar(year, month, 1);

        return cal.getTime();
    }

    public Date getYearBefore(int numbToRed) {
        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.YEAR, numbToRed * -1);
        return cal.getTime();
    }

    public Date toDate(String date) {
        try {
            return Date.from(LocalDate.parse(date).atStartOfDay(ZoneId.systemDefault()).toInstant());
        } catch (DateTimeParseException e) {
            throw new DateFormerException("Invalid input for date parsing ", e);
        }
    }

    public String toString(Date date) {
        return sdf.format(date);
    }


}
