package com.shyrkov.service;

import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.service.model.CurrencyExchangeRate;
import com.shyrkov.service.model.MoneyByCurrency;
import org.springframework.stereotype.Component;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.List;
import java.util.Locale;

@Component
public class CurrencyConverter {

    private final CurrencyExtractor currencyExtractor;

    public CurrencyConverter(CurrencyExtractor currencyExtractor) {
        this.currencyExtractor = currencyExtractor;
    }

    public Double convert(List<MoneyByCurrency> moneyByCurrency, CurrencyEntity desiredCurrency){
        List<CurrencyExchangeRate> exchangeRates = currencyExtractor.extractRates();
        Double moneyInUah = convertToUah(moneyByCurrency, exchangeRates);
        DecimalFormat decimalFormat = new DecimalFormat("#.0000", DecimalFormatSymbols.getInstance(Locale.US));

        double sum = 0.0;
        for (CurrencyExchangeRate exchangeRate : exchangeRates) {
            if(exchangeRate.getCurrency().getCode().equals(desiredCurrency.getCode())){
                sum = moneyInUah/exchangeRate.getExchangeRate();
                break;
            }
        }

        return Double.valueOf(decimalFormat.format(sum));
    }

    private Double convertToUah(List<MoneyByCurrency> moneyByCurrency, List<CurrencyExchangeRate> exchangeRates){
        double sum = 0.0;
        for (CurrencyExchangeRate exchangeRate : exchangeRates) {
            for (MoneyByCurrency money : moneyByCurrency) {
                if(exchangeRate.getCurrency().getCode().equals(money.getCurrency())){
                    sum+=money.getSum()*exchangeRate.getExchangeRate();
                }
            }
        }
        return sum;
    }
}
