package com.shyrkov.service;

import com.shyrkov.persistence.CurrencyDao;
import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.service.mappers.CurrencyMapper;
import com.shyrkov.service.validation.CurrencyValidator;
import com.shyrkov.web.dto.CurrencyDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CurrencyService {

    private final CurrencyDao currencyDao;
    private final CurrencyMapper currencyMapper;
    private final CurrencyValidator currencyValidator;

    public CurrencyService(CurrencyDao currencyDao, CurrencyMapper currencyMapper,
                           CurrencyValidator currencyValidator) {
        this.currencyDao = currencyDao;
        this.currencyMapper = currencyMapper;
        this.currencyValidator = currencyValidator;
    }

    @PreAuthorize("hasRole('ADMIN')")
    public CurrencyDto addNewCurrency(CurrencyDto currencyDto) {
        currencyValidator.validate(currencyDto);

        CurrencyEntity currencyEntity = currencyMapper.dtoToEntity(currencyDto);

        return currencyMapper.entityToDto(currencyDao.save(currencyEntity));
    }

    @PreAuthorize("hasRole('ADMIN')")
    public List<CurrencyDto> getAllCurrencies() {
        List<CurrencyDto> currencyDtos = new ArrayList<>();
        currencyDao.findAll()
                   .stream()
                   .map(currencyMapper::entityToDto)
                   .forEach(currencyDtos::add);
        return currencyDtos;
    }
}
