package com.shyrkov.service.mappers;

import com.shyrkov.persistence.entities.RepairedDetailsEntity;
import com.shyrkov.web.dto.RepairedDetailDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class RepairedDetailsMapper implements DtoToEntityMapper<RepairedDetailDto, RepairedDetailsEntity>,
                                                       EntityToDtoMapper<RepairedDetailsEntity, RepairedDetailDto>{
}
