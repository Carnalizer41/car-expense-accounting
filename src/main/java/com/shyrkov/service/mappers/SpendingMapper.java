package com.shyrkov.service.mappers;

import com.shyrkov.persistence.entities.SpendingEntity;
import com.shyrkov.web.dto.SpendingDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class SpendingMapper implements DtoToEntityMapper<SpendingDto, SpendingEntity>,
                                                EntityToDtoMapper<SpendingEntity, SpendingDto> {
}
