package com.shyrkov.service.mappers;

import com.shyrkov.persistence.entities.RefuelEntity;
import com.shyrkov.web.dto.RefuelDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class RefuelMapper implements DtoToEntityMapper<RefuelDto, RefuelEntity>,
                                              EntityToDtoMapper<RefuelEntity, RefuelDto>{
}
