package com.shyrkov.service.mappers;

import com.shyrkov.persistence.entities.CarEntity;
import com.shyrkov.web.dto.CarInfoDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class CarInfoMapper implements DtoToEntityMapper<CarInfoDto, CarEntity>,
                                               EntityToDtoMapper<CarEntity, CarInfoDto>{
}
