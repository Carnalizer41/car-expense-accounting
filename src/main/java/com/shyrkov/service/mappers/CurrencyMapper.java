package com.shyrkov.service.mappers;

import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.web.dto.CurrencyDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class CurrencyMapper implements DtoToEntityMapper<CurrencyDto, CurrencyEntity>,
                                                EntityToDtoMapper<CurrencyEntity, CurrencyDto> {
}
