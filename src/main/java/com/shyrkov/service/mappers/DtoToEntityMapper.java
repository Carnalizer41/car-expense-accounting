package com.shyrkov.service.mappers;

public interface DtoToEntityMapper<D, E> {

    E dtoToEntity(D dto);

}
