package com.shyrkov.service.mappers;

public interface EntityToDtoMapper<E, D> {

    D entityToDto(E entity);

}
