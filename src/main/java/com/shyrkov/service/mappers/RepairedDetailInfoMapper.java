package com.shyrkov.service.mappers;

import com.shyrkov.persistence.entities.RepairedDetailsEntity;
import com.shyrkov.web.dto.RepairedDetailInfoDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class RepairedDetailInfoMapper
        implements DtoToEntityMapper<RepairedDetailInfoDto, RepairedDetailsEntity>,
                   EntityToDtoMapper<RepairedDetailsEntity, RepairedDetailInfoDto> {
}
