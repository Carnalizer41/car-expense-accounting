package com.shyrkov.service.mappers;

import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.web.dto.UserDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class UserMapper implements DtoToEntityMapper<UserDto, UserEntity>,
                                            EntityToDtoMapper<UserEntity, UserDto>{
}
