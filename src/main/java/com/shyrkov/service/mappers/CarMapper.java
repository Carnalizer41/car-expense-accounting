package com.shyrkov.service.mappers;

import com.shyrkov.persistence.entities.CarEntity;
import com.shyrkov.web.dto.CarDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class CarMapper implements DtoToEntityMapper<CarDto, CarEntity>,
                                           EntityToDtoMapper<CarEntity, CarDto>{
}
