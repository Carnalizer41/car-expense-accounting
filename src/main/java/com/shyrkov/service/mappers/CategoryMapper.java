package com.shyrkov.service.mappers;

import com.shyrkov.persistence.entities.CategoryEntity;
import com.shyrkov.web.dto.CategoryDto;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public abstract class CategoryMapper implements DtoToEntityMapper<CategoryDto, CategoryEntity>,
                                                EntityToDtoMapper<CategoryEntity, CategoryDto>{
}
