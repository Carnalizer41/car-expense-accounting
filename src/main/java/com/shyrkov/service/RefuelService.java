package com.shyrkov.service;

import com.shyrkov.persistence.*;
import com.shyrkov.persistence.entities.RefuelEntity;
import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.service.mappers.RefuelMapper;
import com.shyrkov.service.mappers.SpendingMapper;
import com.shyrkov.service.model.UserModel;
import com.shyrkov.service.validation.CarIdValidator;
import com.shyrkov.service.validation.DateValidator;
import com.shyrkov.service.validation.RefuelValidator;
import com.shyrkov.web.dto.RefuelAmountDto;
import com.shyrkov.web.dto.RefuelDto;
import com.shyrkov.web.dto.SpendingDto;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class RefuelService {

    private final RefuelDao refuelDao;
    private final UserDao userDao;
    private final SpendingMapper spendingMapper;
    private final RefuelMapper refuelMapper;
    private final SpendingService spendingService;
    private final RefuelValidator refuelValidator;
    private final CarIdValidator carIdValidator;
    private final DateValidator dateValidator;

    public RefuelService(RefuelDao refuelDao, UserDao userDao, SpendingMapper spendingMapper,
                         RefuelMapper refuelMapper, SpendingService spendingService,
                         RefuelValidator refuelValidator, CarIdValidator carIdValidator,
                         DateValidator dateValidator) {
        this.refuelDao = refuelDao;
        this.userDao = userDao;
        this.spendingMapper = spendingMapper;
        this.refuelMapper = refuelMapper;
        this.spendingService = spendingService;
        this.refuelValidator = refuelValidator;
        this.carIdValidator = carIdValidator;
        this.dateValidator = dateValidator;
    }

    public RefuelDto addNewRefuel(RefuelDto refuelDto) {
        refuelValidator.validate(refuelDto);

        SpendingDto spendingDto = spendingService.addNewSpending(refuelDto.getSpendingDto());
        refuelDto.setSpendingDto(spendingDto);

        RefuelEntity refuelEntity = refuelMapper.dtoToEntity(refuelDto);
        refuelEntity.setRefuelSpendingEntity(spendingMapper.dtoToEntity(spendingDto));

        RefuelDto savedRefuelDto = refuelMapper.entityToDto(refuelDao.save(refuelEntity));
        savedRefuelDto.setSpendingDto(spendingDto);
        return savedRefuelDto;
    }

    public RefuelAmountDto getRefuelAmountByCar(Long carId, String startDate, String endDate) {
        carIdValidator.validate(carId);
        dateValidator.validate(startDate);
        dateValidator.validate(endDate);

        RefuelAmountDto refuelAmountDto = new RefuelAmountDto();
        refuelAmountDto.setAmount(refuelDao.findAmountByCar(carId, startDate, endDate));

        return refuelAmountDto;
    }

    public RefuelAmountDto getRefuelAmountByUser(String startDate, String endDate){
        dateValidator.validate(startDate);
        dateValidator.validate(endDate);

        RefuelAmountDto refuelAmountDto = new RefuelAmountDto();
        UserModel principal = (UserModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserEntity user = userDao.findOneByEmail(principal.getEmail());

        refuelAmountDto.setAmount(refuelDao.findAmountByUser(user.getId(), startDate, endDate));
        return refuelAmountDto;
    }
}
