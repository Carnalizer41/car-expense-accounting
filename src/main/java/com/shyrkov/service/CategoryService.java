package com.shyrkov.service;

import com.shyrkov.persistence.CategoryDao;
import com.shyrkov.persistence.entities.CategoryEntity;
import com.shyrkov.service.mappers.CategoryMapper;
import com.shyrkov.service.validation.NewCategoryValidator;
import com.shyrkov.web.dto.CategoryDto;
import com.shyrkov.web.dto.UpdateCategoryDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class CategoryService {

    private final CategoryDao categoryDao;
    private final CategoryMapper categoryMapper;
    private final NewCategoryValidator categoryValidator;


    public CategoryService(CategoryDao categoryDao, CategoryMapper categoryMapper,
                           NewCategoryValidator categoryValidator) {
        this.categoryDao = categoryDao;
        this.categoryMapper = categoryMapper;
        this.categoryValidator = categoryValidator;
    }

    @PreAuthorize("hasRole('ADMIN')")
    public CategoryDto addNewCategory(CategoryDto categoryDto) {
        categoryValidator.validate(categoryDto);

        CategoryEntity categoryEntity = categoryMapper.dtoToEntity(categoryDto);

        return categoryMapper.entityToDto(categoryDao.save(categoryEntity));
    }

    @PreAuthorize("hasRole('ADMIN')")
    public CategoryDto updateCategory(UpdateCategoryDto categoryDto) {
        CategoryEntity categoryEntity = categoryDao.findOneByName(categoryDto.getOldName());
        categoryDao.updateCategoryInfo(categoryEntity.getId(), categoryDto.getNewName());
        categoryEntity.setName(categoryDto.getNewName());
        return categoryMapper.entityToDto(categoryEntity);
    }

    public List<CategoryDto> getAllCategories() {
        List<CategoryDto> categoryDtos = new ArrayList<>();
        categoryDao.findAll()
                   .stream()
                   .map(categoryMapper::entityToDto)
                   .forEach(categoryDtos::add);
        return categoryDtos;
    }
}
