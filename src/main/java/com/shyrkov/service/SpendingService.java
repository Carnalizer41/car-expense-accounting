package com.shyrkov.service;

import com.shyrkov.persistence.CarDao;
import com.shyrkov.persistence.CategoryDao;
import com.shyrkov.persistence.CurrencyDao;
import com.shyrkov.persistence.SpendingDao;
import com.shyrkov.persistence.entities.CarEntity;
import com.shyrkov.persistence.entities.CategoryEntity;
import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.persistence.entities.SpendingEntity;
import com.shyrkov.service.mappers.CarInfoMapper;
import com.shyrkov.service.mappers.CategoryMapper;
import com.shyrkov.service.mappers.CurrencyMapper;
import com.shyrkov.service.mappers.SpendingMapper;
import com.shyrkov.service.model.MoneyByCurrency;
import com.shyrkov.service.validation.CarIdValidator;
import com.shyrkov.service.validation.ExpenseInfoValidator;
import com.shyrkov.web.dto.MoneySpentDto;
import com.shyrkov.web.dto.SpendingDto;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
public class SpendingService {

    private final SpendingDao spendingDao;
    private final CarDao carDao;
    private final CarInfoMapper carMapper;
    private final SpendingMapper spendingMapper;
    private final CategoryMapper categoryMapper;
    private final CurrencyMapper currencyMapper;
    private final CategoryDao categoryDao;
    private final CurrencyDao currencyDao;
    private final CurrencyConverter currencyConverter;
    private final ExpenseInfoValidator expenseInfoValidator;
    private final CarIdValidator carIdValidator;

    public SpendingService(SpendingDao spendingDao, CarDao carDao, CarInfoMapper carMapper,
                           SpendingMapper spendingMapper,
                           CategoryMapper categoryMapper, CurrencyMapper currencyMapper,
                           CategoryDao categoryDao,
                           CurrencyDao currencyDao, CurrencyConverter currencyConverter,
                           ExpenseInfoValidator expenseInfoValidator,
                           CarIdValidator carIdValidator) {
        this.spendingDao = spendingDao;
        this.carDao = carDao;
        this.carMapper = carMapper;
        this.spendingMapper = spendingMapper;
        this.categoryMapper = categoryMapper;
        this.currencyMapper = currencyMapper;
        this.categoryDao = categoryDao;
        this.currencyDao = currencyDao;
        this.currencyConverter = currencyConverter;
        this.expenseInfoValidator = expenseInfoValidator;
        this.carIdValidator = carIdValidator;
    }

    public SpendingDto addNewSpending(SpendingDto spendingDto){
        expenseInfoValidator.validate(spendingDto);

        SpendingEntity spendingEntity = spendingMapper.dtoToEntity(spendingDto);
        CategoryEntity categoryEntity = categoryDao
                .findOneByName(spendingDto.getCategoryDto().getName());

        spendingEntity.setCategoryEntity(categoryEntity);
        CarEntity carEntity = carDao.findOneById(spendingDto.getCarInfoDto().getId());
        spendingEntity.setCarEntity(carEntity);
        CurrencyEntity currencyEntity = currencyDao.findOneByCode(spendingDto.getCurrency().getCode());
        spendingEntity.setCurrencyEntity(currencyEntity);

        SpendingEntity savedSpendingEntity = spendingDao.saveAndFlush(spendingEntity);

        SpendingDto newSpendingDto = spendingMapper.entityToDto(savedSpendingEntity);
        newSpendingDto.setCategoryDto(categoryMapper.entityToDto(categoryEntity));
        newSpendingDto.setCarInfoDto(carMapper.entityToDto(carEntity));
        newSpendingDto.setCurrency(currencyMapper.entityToDto(currencyEntity));

        return newSpendingDto;
    }

    public MoneySpentDto getMoneySpentOnCarByCategory(Long carId, String category, String desiredCurrency){
        carIdValidator.validate(carId);

        MoneySpentDto moneySpentDto = new MoneySpentDto();
        CategoryEntity categoryEntity = categoryDao.findOneByName(category);

        List<MoneyByCurrency> moneyByCategory = spendingDao.getMoneyByCategoryAndCar(categoryEntity.getId(), carId);

        CurrencyEntity currencyEntity = currencyDao.findOneByCode(desiredCurrency);
        moneySpentDto.setMoneySpent(currencyConverter.convert(moneyByCategory, currencyEntity));
        return moneySpentDto;
    }

    public List<SpendingDto> getHistoryByCategory(String category, Long carId){
        carIdValidator.validate(carId);

        List<SpendingDto> spendingDtos = new ArrayList<>();
        CategoryEntity categoryEntity = categoryDao.findOneByName(category);

        for (SpendingEntity spendingEntity : spendingDao.findByCategoryEntityIdAndCarEntityId(categoryEntity.getId(), carId)) {
            SpendingDto spendingDto = spendingMapper.entityToDto(spendingEntity);
            spendingDto.setCurrency(currencyMapper.entityToDto(spendingEntity.getCurrencyEntity()));
            spendingDto.setCarInfoDto(carMapper.entityToDto(spendingEntity.getCarEntity()));
            spendingDto.setCategoryDto(categoryMapper.entityToDto(spendingEntity.getCategoryEntity()));
            spendingDtos.add(spendingDto);
        }

        return spendingDtos;
    }

}
