package com.shyrkov.service;

import com.shyrkov.persistence.CurrencyDao;
import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.exceptions.ParserException;
import com.shyrkov.service.model.CurrencyExchangeRate;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Attribute;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CurrencyExtractor {

    private final String exchangeRateUrl;
    private final CurrencyDao currencyDao;

    public CurrencyExtractor(@Value("${currency.exchange.rates.url}") String exchangeRateUrl,
                             CurrencyDao currencyDao) {
        this.exchangeRateUrl = exchangeRateUrl;
        this.currencyDao = currencyDao;
    }

    public List<CurrencyExchangeRate> extractRates(){
        List<CurrencyExchangeRate> exchangeRates = new ArrayList<>();

        exchangeRates.add(new CurrencyExchangeRate(currencyDao.findOneByCode("UAH"), 1.0));

        for (Map.Entry<CurrencyEntity, Document> entry : getPagesByCurrency().entrySet()) {
            CurrencyExchangeRate exchangeRate = new CurrencyExchangeRate();
            exchangeRate.setCurrency(entry.getKey());

            Elements elements = entry.getValue().getElementsByClass("mfm-text-nowrap");
            Elements exchangeRateElements = new Elements();

            for (Element element : elements) {
                for (Attribute attribute : element.attributes()) {
                    if(attribute.getValue().equals("Курс гривні")){
                        exchangeRateElements.add(element);
                    }
                }
            }
            String[] s = exchangeRateElements.first().text().split(" ");
            exchangeRate.setExchangeRate(Double.parseDouble(s[0]));

            exchangeRates.add(exchangeRate);
        }
        return exchangeRates;
    }

    private Map<CurrencyEntity, Document> getPagesByCurrency(){
        Map<CurrencyEntity, Document> pages = new HashMap<>();
        for (CurrencyEntity currency : currencyDao.findAll()) {
            if(currency.getCode().equals("UAH"))
                continue;
            String url = exchangeRateUrl+currency.getCode();
            try {

                pages.put(currency, Jsoup.connect(url).get());
            } catch (IOException e) {
                throw new ParserException("Can`t connect to exchange rates web page", e);
            }
        }
        return pages;
    }

}
