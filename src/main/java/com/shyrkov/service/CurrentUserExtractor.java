package com.shyrkov.service;

import com.shyrkov.persistence.UserDao;
import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.service.model.UserModel;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

@Component
public class CurrentUserExtractor {

    private final UserDao userDao;

    public CurrentUserExtractor(UserDao userDao) {
        this.userDao = userDao;
    }

    public UserEntity getCurrentUser(){
        UserModel principal = (UserModel) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userDao.findOneByEmail(principal.getEmail());
    }

}
