package com.shyrkov.service.model;

public interface CarsPopularity {

    String getName();
    Integer getAmount();
}
