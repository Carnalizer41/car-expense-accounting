package com.shyrkov.service.model;

import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.enums.UserRole;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class UserRoleModel {

    private Long id;
    private UserRole role;
    private UserEntity user;
}
