package com.shyrkov.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CarsPopularityImpl {

    private String name;
    private Integer amount;
    private String percentAll;
    private String percentMark;
}
