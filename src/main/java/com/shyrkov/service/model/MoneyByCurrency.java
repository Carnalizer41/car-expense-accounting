package com.shyrkov.service.model;

public interface MoneyByCurrency {

    String getCurrency();
    Double getSum();
}
