package com.shyrkov.service.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CarsReliability {

    private String name;
    private Integer amount;
    private String percentAll;
    private String reliabilityCoefficient;
}
