package com.shyrkov.service.model;

import com.shyrkov.persistence.entities.CurrencyEntity;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CurrencyExchangeRate {

    private CurrencyEntity currency;
    private Double exchangeRate;

    public CurrencyExchangeRate(CurrencyEntity currency, Double exchangeRate) {
        this.currency = currency;
        this.exchangeRate = exchangeRate;
    }

    public CurrencyExchangeRate() {
    }
}
