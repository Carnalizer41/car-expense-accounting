package com.shyrkov.service.model;

public interface RepairedDetailStatistics {

    String getName();
    Integer getAmount();
    Double getCost();
}
