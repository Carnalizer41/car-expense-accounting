package com.shyrkov.service.model;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class UserModel {

    private Long id;
    private String name;
    private String email;
    private String phone;
    private String password;
    private List<UserRoleModel> roles;

    public void addRole(UserRoleModel userRole) {
        roles.add(userRole);
    }
}
