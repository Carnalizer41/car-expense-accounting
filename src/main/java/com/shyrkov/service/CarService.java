package com.shyrkov.service;

import com.shyrkov.exceptions.EntityNotFoundException;
import com.shyrkov.persistence.CarDao;
import com.shyrkov.persistence.RepairedDetailsDao;
import com.shyrkov.persistence.entities.CarEntity;
import com.shyrkov.persistence.entities.RepairedDetailsEntity;
import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.service.mappers.CarInfoMapper;
import com.shyrkov.service.mappers.CarMapper;
import com.shyrkov.service.mappers.CurrencyMapper;
import com.shyrkov.service.mappers.UserMapper;
import com.shyrkov.service.model.CarsPopularity;
import com.shyrkov.service.model.CarsPopularityImpl;
import com.shyrkov.service.model.CarsReliability;
import com.shyrkov.service.validation.CarIdValidator;
import com.shyrkov.service.validation.CarValidator;
import com.shyrkov.web.dto.CarDto;
import com.shyrkov.web.dto.CarInfoDto;
import com.shyrkov.web.dto.CrashStatisticsDto;
import com.shyrkov.web.dto.UserDto;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Component;

import java.util.*;

@Component
public class CarService {

    private final CarDao carDao;
    private final RepairedDetailsDao repairedDetailsDao;
    private final CarMapper carMapper;
    private final CarInfoMapper carInfoMapper;
    private final UserMapper userMapper;
    private final CurrentUserExtractor userExtractor;
    private final CurrencyMapper currencyMapper;
    private final CrashAnalyzer crashAnalyzer;
    private final CarValidator carValidator;
    private final CarIdValidator carIdValidator;
    private final UserService userService;

    public CarService(CarDao carDao, RepairedDetailsDao repairedDetailsDao,
            CarMapper carMapper, CarInfoMapper carInfoMapper,
            UserMapper userMapper, CurrentUserExtractor userExtractor,
            CurrencyMapper currencyMapper, CrashAnalyzer crashAnalyzer,
            CarValidator carValidator, CarIdValidator carIdValidator,
            UserService userService) {
        this.carDao = carDao;
        this.repairedDetailsDao = repairedDetailsDao;
        this.carMapper = carMapper;
        this.carInfoMapper = carInfoMapper;
        this.userMapper = userMapper;
        this.userExtractor = userExtractor;
        this.currencyMapper = currencyMapper;
        this.crashAnalyzer = crashAnalyzer;
        this.carValidator = carValidator;
        this.carIdValidator = carIdValidator;
        this.userService = userService;
    }

    public CarEntity findById(Long id) {
        return carDao.findOneById(id);
    }

    public CarDto addNewCar(CarDto carDto) {
        carValidator.validate(carDto);
        carValidator.checkExistence(carDto);

        CarEntity carEntity = carMapper.dtoToEntity(carDto);
        //        UserEntity userEntity = userExtractor.getCurrentUser();
        UserEntity userEntity = new UserEntity();
        userEntity.setId(1L);

        carEntity.setUserEntity(userEntity);
        CarDto newCar = carMapper.entityToDto(carDao.save(carEntity));

        UserDto userDto = userMapper.entityToDto(carEntity.getUserEntity());
        userDto.setCurrency(currencyMapper.entityToDto(
                carEntity.getUserEntity().getCurrency()));
        newCar.setUserEntity(userDto);
        return newCar;
    }

    public List<CarInfoDto> getUsersCars() {
        UserEntity user = userExtractor.getCurrentUser();

        List<CarInfoDto> carInfos = new ArrayList<>();
        for (CarEntity carEntity : carDao.findAll()) {
            if (user.getId().equals(carEntity.getUserEntity().getId()))
                carInfos.add(carInfoMapper.entityToDto(carEntity));
        }

        return carInfos;
    }

    public CarInfoDto removeCar(CarDto carDto) {
        carIdValidator.validate(carDto.getId());

        List<CarEntity> carsByUser = carDao.findByUser(
                userExtractor.getCurrentUser().getId());
        CarEntity targetCar = carDao.findOneById(carDto.getId());
        boolean isDeleted = false;
        for (CarEntity carEntity : carsByUser) {
            if (carEntity.getVinCode().equals(targetCar.getVinCode())) {
                carDao.deleteOneById(targetCar.getId());
                isDeleted = true;
                break;
            }
        }
        if (!isDeleted)
            throw new EntityNotFoundException(
                    "There is no such car owned by current user");

        return carInfoMapper.entityToDto(targetCar);
    }

    public CarInfoDto editCarInfo(CarDto carDto) {
        carIdValidator.validate(carDto.getId());
        carValidator.validate(carDto);

        UserEntity currentUser = userExtractor.getCurrentUser();
        List<CarEntity> carsByUser = carDao.findByUser(currentUser.getId());
        CarEntity targetCar = carMapper.dtoToEntity(carDto);

        for (CarEntity carEntity : carsByUser) {
            if (carEntity.getId().equals(targetCar.getId())) {
                carDao.editCarInfo(currentUser.getId(), targetCar.getId(),
                        targetCar.getVinCode(), targetCar.getMark(),
                        targetCar.getModel(), targetCar.getColor());
                break;
            }
        }
        return carInfoMapper.entityToDto(targetCar);
    }

    @PreAuthorize("hasRole('ADMIN')")
    public Map<String, CrashStatisticsDto> getCarRating() {
        Map<String, CrashStatisticsDto> crashStatisticsDtoMap = new HashMap<>();
        Map<String, List<RepairedDetailsEntity>> repairsByMarks = new HashMap<>();
        for (RepairedDetailsEntity repair : repairedDetailsDao.findAll()) {
            String mark = repair.getDetailsSpendingEntity().getCarEntity()
                    .getMark();
            if (!repairsByMarks.containsKey(mark)) {
                repairsByMarks.put(mark,
                        new ArrayList<>(Arrays.asList(repair)));
            } else {
                repairsByMarks.computeIfPresent(mark, (k, v) -> {
                    v.add(repair);
                    return v;
                });
            }
        }
        for (String key : repairsByMarks.keySet()) {
            crashStatisticsDtoMap.put(key,
                    crashAnalyzer.getCrashStatistics(repairsByMarks.get(key)));
        }
        return crashStatisticsDtoMap;
    }

    @PreAuthorize("hasRole('ADMIN')")
    public List<CarsPopularityImpl> getCarPopularity() {
        List<CarsPopularityImpl> carsPopularity = new ArrayList<>();
        for (CarsPopularity popularity : carDao.getCarsPopularity()) {
            carsPopularity.add(new CarsPopularityImpl(popularity.getName(),
                    popularity.getAmount(), String.format("%.2f",
                    ((double) popularity.getAmount() / 2218 * 100)), null));
        }
        return carsPopularity;
    }

    @PreAuthorize("hasRole('ADMIN')")
    public List<CarsPopularityImpl> getCarModelPopularity(String mark) {
        List<CarsPopularityImpl> carsPopularity = new ArrayList<>();
        for (CarsPopularity popularity : carDao.getCarsModelPopularity(mark)) {
            carsPopularity.add(new CarsPopularityImpl(popularity.getName(),
                    popularity.getAmount(), String.format("%.2f",
                    ((double) popularity.getAmount() / 2218 * 100)),
                    String.format("%.2f", ((double) popularity.getAmount()
                            / carDao.getAmountByMark(mark) * 100))));
        }
        return carsPopularity;
    }

    @PreAuthorize("hasRole('ADMIN')")
    public List<CarsReliability> getCarReliability() {
        List<CarsReliability> carsPopularity = new ArrayList<>();
        for (CarsPopularity popularity : carDao.getCarsPopularity()) {
            carsPopularity.add(new CarsReliability(popularity.getName(),
                    popularity.getAmount(), String.format("%.2f",
                    ((double) popularity.getAmount() / 2218 * 100)),
                    String.format("%.2f",
                            ((double) repairedDetailsDao.getRepairsAmountByMark(
                                    popularity.getName())) / 89
                                    / carDao.getAmountByMark(
                                    popularity.getName()) * 100)));
        }
        return carsPopularity;
    }

    @PreAuthorize("hasRole('ADMIN')")
    public Map<String, CrashStatisticsDto> getCarModelRating() {
        Map<String, CrashStatisticsDto> crashStatisticsDtoMap = new HashMap<>();
        Map<String, List<RepairedDetailsEntity>> repairsByMarks = new HashMap<>();
        for (RepairedDetailsEntity repair : repairedDetailsDao.findAll()) {
            String mark =
                    repair.getDetailsSpendingEntity().getCarEntity().getMark()
                            + " " + repair.getDetailsSpendingEntity()
                            .getCarEntity().getModel();
            if (!repairsByMarks.containsKey(mark)) {
                repairsByMarks.put(mark,
                        new ArrayList<>(Arrays.asList(repair)));
            } else {
                repairsByMarks.computeIfPresent(mark, (k, v) -> {
                    v.add(repair);
                    return v;
                });
            }
        }
        for (String key : repairsByMarks.keySet()) {
            crashStatisticsDtoMap.put(key,
                    crashAnalyzer.getCrashStatistics(repairsByMarks.get(key)));
        }
        return crashStatisticsDtoMap;
    }
}
