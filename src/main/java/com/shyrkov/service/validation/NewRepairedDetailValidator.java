package com.shyrkov.service.validation;

import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.persistence.RepairedDetailsDao;
import com.shyrkov.web.dto.RepairedDetailDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.*;

@Component
public class NewRepairedDetailValidator implements Validator<RepairedDetailDto> {

    private final ExpenseInfoValidator infoValidator;
    private final RepairedDetailsDao repairedDetailsDao;

    public NewRepairedDetailValidator(ExpenseInfoValidator infoValidator,
                                      RepairedDetailsDao repairedDetailsDao) {
        this.infoValidator = infoValidator;
        this.repairedDetailsDao = repairedDetailsDao;
    }

    @Override
    public void validate(@NotNull RepairedDetailDto newRepairedDetailDto) {
        if (isBlankOrNull(newRepairedDetailDto.getSerialNumber())) {
            throw new ValidationException("Replaced part's serial number is missing");
        }

        if (isBlankOrNull(newRepairedDetailDto.getName())) {
            throw new ValidationException("Replaced part's name is missing");
        }

        if (newRepairedDetailDto.getSpendingDto() == null) {
            throw new ValidationException("Expense info is missing");
        }

        if (!minLength(newRepairedDetailDto.getSerialNumber(), 4) ||
            !matchRegexp(newRepairedDetailDto.getSerialNumber(), ONLY_NUMBERS_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid serial number format");
        }

        infoValidator.validate(newRepairedDetailDto.getSpendingDto());

        if (repairedDetailsDao.existsBySerialNumber(newRepairedDetailDto.getSerialNumber())) {
            throw new NotUniqueEntityException("Replaced part with such a serial number already exists");
        }
    }
}
