package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;

import static org.apache.commons.validator.GenericValidator.*;

public interface EmailPasswordValidator {

    default void validateEmail(String email) {
        if (isBlankOrNull(email)) throw new ValidationException("Email is missing");
        if (!isEmail(email)) throw new ValidationException("Invalid email format");
    }

    default void validatePassword(String password) {
        if (isBlankOrNull(password)) throw new ValidationException("Password is missing");
        if (!minLength(password, 6)) throw new ValidationException("Too short password");
    }
}
