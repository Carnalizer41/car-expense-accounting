package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.dto.CurrencyDto;
import org.springframework.stereotype.Component;

import static org.apache.commons.validator.GenericValidator.*;

@Component
public class CurrencyValidator implements Validator<CurrencyDto> {

    @Override
    public void validate(CurrencyDto currency) {
        if (currency == null) {
            throw new ValidationException("Currency is missing");
        }
        if (isBlankOrNull(currency.getName())) {
            throw new ValidationException("Currency's name is missing");
        }

        if (isBlankOrNull(currency.getCode())) {
            throw new ValidationException("Currency's code type is missing");
        }

        if (!minLength(currency.getName(), 4) || !matchRegexp(currency.getName(), ONLY_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid currency's name format");
        }

        if (!minLength(currency.getCode(), 2) || !matchRegexp(currency.getCode(), ONLY_LETTERS)) {
            throw new ValidationException("Invalid currency's code format");
        }
    }
}
