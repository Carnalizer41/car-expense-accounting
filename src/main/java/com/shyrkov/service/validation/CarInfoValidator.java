package com.shyrkov.service.validation;

import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.persistence.CarDao;
import com.shyrkov.web.dto.CarInfoDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.*;

@Component
public class CarInfoValidator implements Validator<CarInfoDto> {

    private final CarDao carInfoDao;

    public CarInfoValidator(CarDao carInfoDao) {
        this.carInfoDao = carInfoDao;
    }

    @Override
    public void validate(@NotNull CarInfoDto carInfo) {
        if (isBlankOrNull(carInfo.getVinCode())) {
            throw new ValidationException("Car's vin code is missing");
        }

        if (isBlankOrNull(carInfo.getModel())) {
            throw new ValidationException("Car's model is missing");
        }

        if (isBlankOrNull(carInfo.getColor())) {
            throw new ValidationException("Car's color is missing");
        }

        if (isBlankOrNull(carInfo.getMark())) {
            throw new ValidationException("Car's mark is missing");
        }

        if (!minLength(carInfo.getVinCode(), 4) || !matchRegexp(carInfo.getVinCode(), ONLY_NUMBERS_AND_LETTERS)) {
            throw new ValidationException("Invalid car's vin code format");
        }

        if (!minLength(carInfo.getModel(), 2) || !matchRegexp(carInfo.getModel(), ONLY_NUMBERS_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid car's model format");
        }

        if (!minLength(carInfo.getColor(), 3) || !matchRegexp(carInfo.getColor(), ONLY_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid car's color format");
        }

        if (!minLength(carInfo.getMark(), 3) || !matchRegexp(carInfo.getMark(), ONLY_NUMBERS_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid car's mark format");
        }

        if (carInfoDao.existsByVinCode(carInfo.getVinCode())) {
            throw new NotUniqueEntityException("Car with such vin code already exists");
        }
    }
}
