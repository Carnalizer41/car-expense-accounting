package com.shyrkov.service.validation;

import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.persistence.CategoryDao;
import com.shyrkov.web.dto.CategoryDto;
import org.springframework.stereotype.Component;

@Component
public class NewCategoryValidator implements Validator<CategoryDto> {

    private final CategoryValidator categoryValidator;
    private final CategoryDao categoryDao;

    public NewCategoryValidator(CategoryValidator categoryValidator,
                                CategoryDao categoryDao) {
        this.categoryValidator = categoryValidator;
        this.categoryDao = categoryDao;
    }

    @Override
    public void validate(CategoryDto category) {
        categoryValidator.validate(category);

        if (categoryDao.existsByExpenseType(category.getName())) {
            throw new NotUniqueEntityException("Such an expense category already exists");
        }
    }
}
