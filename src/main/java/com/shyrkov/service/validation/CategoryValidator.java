package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.dto.CategoryDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.*;

@Component
public class CategoryValidator implements Validator<CategoryDto> {

    @Override
    public void validate(@NotNull CategoryDto category) {
        if (isBlankOrNull(category.getName())) {
            throw new ValidationException("Expense type is missing");
        }

        if (!minLength(category.getName(), 3) || !matchRegexp(category.getName(), ONLY_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid expense type format");
        }
    }
}
