package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import org.springframework.stereotype.Component;

import static org.apache.commons.validator.GenericValidator.isBlankOrNull;
import static org.apache.commons.validator.GenericValidator.isDate;

@Component
public class DateValidator implements Validator<String> {

    @Override
    public void validate(String date) {
        if (isBlankOrNull(date)) {
            throw new ValidationException("Date is missing");
        }

        if (!isDate(date, "yyyy-MM-dd", true)) {
            throw new ValidationException("Invalid date format");
        }
    }
}
