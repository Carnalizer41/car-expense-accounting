package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.dto.RefuelDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.minValue;

@Component
public class RefuelValidator implements Validator<RefuelDto> {

    private final ExpenseInfoValidator infoValidator;

    public RefuelValidator(ExpenseInfoValidator infoValidator) {
        this.infoValidator = infoValidator;
    }

    @Override
    public void validate(@NotNull RefuelDto refuel) {

        if (refuel.getAmount() == null || !minValue(refuel.getAmount(), 0.01)) {
            throw new ValidationException("Refuel volume is missing");
        }

        if (refuel.getSpendingDto() == null) {
            throw new ValidationException("Refuel info is missing");
        }

        infoValidator.validate(refuel.getSpendingDto());
    }
}
