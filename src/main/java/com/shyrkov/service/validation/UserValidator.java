package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.persistence.UserDao;
import com.shyrkov.web.dto.CreateUserDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.*;

@Component
public class UserValidator implements Validator<CreateUserDto>, EmailPasswordValidator {

    private final UserDao userDao;

    public UserValidator(UserDao userDao) {
        this.userDao = userDao;
    }

    @Override
    public void validate(@NotNull CreateUserDto user) {

        validateEmail(user.getEmail());
        validatePassword(user.getPassword());

        if (isBlankOrNull(user.getName())) {
            throw new ValidationException("User's name is missing");
        }

        if (isBlankOrNull(user.getPhone())) {
            throw new ValidationException("User's phone is missing");
        }

        if (!minLength(user.getName(), 2) || !matchRegexp(user.getName(), ONLY_LETTERS_AND_SPACE)) {
            throw new ValidationException("Invalid user's name format");
        }

        if (!minLength(user.getPhone(), 10) || !maxLength(user.getPhone(), 13)) {
            throw new ValidationException("Invalid user's phone format");
        }


    }
}

