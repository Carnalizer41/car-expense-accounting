package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.dto.SpendingDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

import static org.apache.commons.validator.GenericValidator.isBlankOrNull;
import static org.apache.commons.validator.GenericValidator.minValue;

@Component
public class ExpenseInfoValidator implements Validator<SpendingDto> {

    private final CategoryValidator categoryValidator;
    private final CarInfoValidator carValidator;

    public ExpenseInfoValidator(CategoryValidator categoryValidator,
                                CarInfoValidator carValidator) {
        this.categoryValidator = categoryValidator;
        this.carValidator = carValidator;
    }

    @Override
    public void validate(@NotNull SpendingDto info) {
        if (info.getDate() == null) {
            throw new ValidationException("Expense date is missing");
        }

        if (info.getSum() == null || !minValue(info.getSum(), 0.01)) {
            throw new ValidationException("Expense sum is missing");
        }

        if (info.getCurrency() == null) {
            throw new ValidationException("Currency is missing");
        }
        if (isBlankOrNull(info.getDescription())) {
            throw new ValidationException("Expense description is missing");
        }

        if (info.getCategoryDto() == null) {
            throw new ValidationException("Expense category is missing");
        }
        if (info.getCarInfoDto() == null) {
            throw new ValidationException("Car is missing");
        }

        categoryValidator.validate(info.getCategoryDto());
    }
}
