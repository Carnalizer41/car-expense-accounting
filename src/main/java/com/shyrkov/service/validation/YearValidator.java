package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import org.springframework.stereotype.Component;


@Component
public class YearValidator implements Validator<Integer> {

    @Override
    public void validate(Integer year) {
        if (year == null) {
            throw new ValidationException("Year is missing");
        }

        if (year < 1900 || year > 2100) {
            throw new ValidationException("Year is out of range");
        }
    }
}
