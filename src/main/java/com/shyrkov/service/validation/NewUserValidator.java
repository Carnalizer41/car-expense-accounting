package com.shyrkov.service.validation;

import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.persistence.UserDao;
import com.shyrkov.web.dto.CreateUserDto;
import org.springframework.stereotype.Component;

import javax.validation.constraints.NotNull;

@Component
public class NewUserValidator implements Validator<CreateUserDto>, EmailPasswordValidator {

    private final UserDao userDao;
    private final UserValidator userValidator;

    public NewUserValidator(UserDao userDao, UserValidator userValidator) {
        this.userDao = userDao;
        this.userValidator = userValidator;
    }

    @Override
    public void validate(@NotNull CreateUserDto newUser) {
        userValidator.validate(newUser);

        if (userDao.existsByEmail(newUser.getEmail())) {
            throw new NotUniqueEntityException("User with such email already exists");
        }
    }
}
