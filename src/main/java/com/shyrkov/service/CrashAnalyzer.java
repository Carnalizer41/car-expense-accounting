package com.shyrkov.service;

import com.shyrkov.persistence.entities.RepairedDetailsEntity;
import com.shyrkov.web.dto.CrashStatisticsDto;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
class CrashAnalyzer {

    CrashStatisticsDto getCrashStatistics(List<RepairedDetailsEntity> repairs) {
        CrashStatisticsDto crashStatistics = new CrashStatisticsDto();
        Map<String, Integer> crashesAmountPerYear = getCrashesAmountPerYear(repairs);
        crashStatistics.setCrashesPerYear(
                crashesAmountPerYear.values().stream().reduce(0, Integer::sum) / crashesAmountPerYear.size());
        Map<String, Integer> crashesAmountPerMonth = getCrashesAmountPerMonth(repairs);
        crashStatistics.setCrashesPerMonth(
                crashesAmountPerMonth.values().stream().reduce(0, Integer::sum) / crashesAmountPerMonth.size());
        Map<String, Double> repairCostPerYear = getRepairCostPerYear(repairs);
        crashStatistics.setRepairCostInYear(String.format("%.2f", (double)
                repairCostPerYear.values().stream().reduce(0.0, Double::sum) / repairCostPerYear.size()/2218));
        Map<String, Double> repairCostPerMonth = getRepairCostPerMonth(repairs);
        crashStatistics.setRepairCostInMonth(String.format("%.2f", (double)
                repairCostPerMonth.values().stream().reduce(0.0, Double::sum) / repairCostPerMonth.size()/ 2218));
        return crashStatistics;
    }

    private Map<String, Integer> getCrashesAmountPerYear(List<RepairedDetailsEntity> repairs) {
        Map<String, Integer> crashesPerYear = new HashMap<>();
        for (RepairedDetailsEntity repair : repairs) {
            final String year = repair.getDetailsSpendingEntity().getDate().substring(0, 4);
            crashesPerYear.put(year, crashesPerYear.getOrDefault(year, 0) + 1);
        }
        return crashesPerYear;
    }

    private Map<String, Integer> getCrashesAmountPerMonth(List<RepairedDetailsEntity> repairs) {
        Map<String, Integer> crashesPerMonth = new HashMap<>();
        for (RepairedDetailsEntity repair : repairs) {
            final String month = repair.getDetailsSpendingEntity().getDate().substring(0, 7);
            crashesPerMonth.put(month, crashesPerMonth.getOrDefault(month, 0) + 1);
        }
        return crashesPerMonth;
    }

    private Map<String, Double> getRepairCostPerYear(List<RepairedDetailsEntity> repairs) {
        Map<String, Double> costPerYear = new HashMap<>();
        for (RepairedDetailsEntity repair : repairs) {
            final String year = repair.getDetailsSpendingEntity().getDate().substring(0, 4);
            final Double cost = repair.getDetailsSpendingEntity().getSum();
            costPerYear.put(year, costPerYear.getOrDefault(year, 0.0) + cost);
        }
        return costPerYear;
    }

    private Map<String, Double> getRepairCostPerMonth(List<RepairedDetailsEntity> repairs) {
        Map<String, Double> costPerMonth = new HashMap<>();
        for (RepairedDetailsEntity repair : repairs) {
            final String month = repair.getDetailsSpendingEntity().getDate().substring(0, 7);
            final Double cost = repair.getDetailsSpendingEntity().getSum();
            costPerMonth.put(month, costPerMonth.getOrDefault(month, 0.0) + cost);
        }
        return costPerMonth;
    }


}
