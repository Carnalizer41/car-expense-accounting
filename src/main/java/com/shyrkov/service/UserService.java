package com.shyrkov.service;

import com.shyrkov.persistence.CurrencyDao;
import com.shyrkov.persistence.UserDao;
import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.persistence.entities.UserRoleEntity;
import com.shyrkov.enums.UserRole;
import com.shyrkov.exceptions.EntityNotFoundException;
import com.shyrkov.service.mappers.CurrencyMapper;
import com.shyrkov.service.mappers.UserMapper;
import com.shyrkov.service.validation.NewUserValidator;
import com.shyrkov.service.validation.UserValidator;
import com.shyrkov.web.dto.CreateUserDto;
import com.shyrkov.web.dto.UserDto;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserService implements UserDetailsService {

    private final UserDao userDao;
    private final BCryptPasswordEncoder bCryptPasswordEncoder;
    private final UserMapper userMapper;
    private final CurrencyMapper currencyMapper;
    private final CurrencyDao currencyDao;
    private final CurrentUserExtractor userExtractor;
    private final NewUserValidator newUserValidator;
    private final UserValidator userValidator;

    public UserService(UserDao userDao,
                       BCryptPasswordEncoder bCryptPasswordEncoder, UserMapper userMapper,
                       CurrencyMapper currencyMapper, CurrencyDao currencyDao,
                       CurrentUserExtractor userExtractor, NewUserValidator newUserValidator,
                       UserValidator userValidator) {
        this.userDao = userDao;
        this.bCryptPasswordEncoder = bCryptPasswordEncoder;
        this.userMapper = userMapper;
        this.currencyMapper = currencyMapper;
        this.currencyDao = currencyDao;
        this.userExtractor = userExtractor;
        this.newUserValidator = newUserValidator;
        this.userValidator = userValidator;
    }

    public UserDto saveUser(CreateUserDto userDto) {
        newUserValidator.validate(userDto);

        final UserDto userDto1 = mapCreateUserToUserDto(userDto);
        UserEntity userEntity = userMapper.dtoToEntity(userDto1);
        userEntity.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        UserRoleEntity userRoleEntity = new UserRoleEntity();
        userRoleEntity.setRole(UserRole.USER);
        userRoleEntity.setUser(userEntity);

        userEntity.setUserRoles(Collections.singletonList(userRoleEntity));

        CurrencyEntity currency = currencyDao.findOneByCode(userDto.getCurrency());
        userEntity.setCurrency(currency);

        UserDto returnUserDto = userMapper.entityToDto(userDao.save(userEntity));
        returnUserDto.setCurrency(currencyMapper.entityToDto(currency));

        return returnUserDto;
    }

    public UserDto editUserInfo(CreateUserDto userDto) {
        userValidator.validate(userDto);

        UserEntity userEntity = userMapper.dtoToEntity(mapCreateUserToUserDto(userDto));
        userEntity.setId(userExtractor.getCurrentUser().getId());
        userEntity.setPassword(bCryptPasswordEncoder.encode(userDto.getPassword()));
        CurrencyEntity currency = currencyDao.findOneByCode(userDto.getCurrency());
        userEntity.setCurrency(currency);

        userDao.updateUserInfo(userEntity.getName(), userEntity.getEmail(), userEntity.getPhone(),
                               userEntity.getCurrency(), userEntity.getId(), userEntity.getPassword());
        UserDto returnUserDto = userMapper.entityToDto(userEntity);
        returnUserDto.setCurrency(currencyMapper.entityToDto(userEntity.getCurrency()));

        return returnUserDto;
    }

    public UserDto getUserInfo() {
        UserEntity userEntity = userExtractor.getCurrentUser();
        UserDto userDto = userMapper.entityToDto(userEntity);
        userDto.setCurrency(currencyMapper.entityToDto(userEntity.getCurrency()));

        return userDto;
    }

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity userEntity = userDao.findOneByEmail(s);
        if (userEntity == null) {
            throw new EntityNotFoundException("User does not exists");
        }

        List<GrantedAuthority> authorities = userEntity.getUserRoles().stream().map(this::toAuthority)
                                                       .collect(Collectors.toList());

        return new User(userEntity.getEmail(), userEntity.getPassword(), authorities);
    }

    private GrantedAuthority toAuthority(UserRoleEntity userRoleEntity) {
        return new SimpleGrantedAuthority(userRoleEntity.getRole().name());
    }

    private UserDto mapCreateUserToUserDto(CreateUserDto createUserDto){
        UserDto userDto = new UserDto();
        userDto.setId(createUserDto.getId());
        userDto.setName(createUserDto.getName());
        userDto.setEmail(createUserDto.getEmail());
        userDto.setPhone(createUserDto.getPhone());
        userDto.setCurrency(currencyMapper.entityToDto(currencyDao.findOneByCode(createUserDto.getCurrency())));
        return userDto;
    }
}
