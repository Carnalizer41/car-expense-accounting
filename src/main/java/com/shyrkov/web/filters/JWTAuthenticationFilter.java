package com.shyrkov.web.filters;

import com.auth0.jwt.JWT;
import com.auth0.jwt.algorithms.Algorithm;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shyrkov.enums.UserRole;
import com.shyrkov.exceptions.AuthException;
import com.shyrkov.service.model.UserModel;
import com.shyrkov.service.model.UserRoleModel;
import com.shyrkov.web.dto.LoginUserDto;
import org.springframework.http.HttpHeaders;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletInputStream;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter {

    private final AuthenticationManager authenticationManager;
    private final String secret;

    public JWTAuthenticationFilter(AuthenticationManager authenticationManager, String secret) {
        this.authenticationManager = authenticationManager;
        this.secret = secret;
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response)
            throws AuthenticationException {
        try (ServletInputStream is = request.getInputStream()) {

            ObjectMapper objectMapper = new ObjectMapper();
            LoginUserDto loginUserDto = objectMapper.readValue(is, LoginUserDto.class);

            Authentication authentication = new UsernamePasswordAuthenticationToken(loginUserDto.getEmail(),
                                                                                    loginUserDto.getPassword(),
                                                                                    Collections.emptyList());
            return authenticationManager.authenticate(authentication);
        } catch (IOException e) {
            throw new AuthException(e.getMessage(), e);
        }
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest request, HttpServletResponse response, FilterChain chain,
                                            Authentication authResult) throws IOException, ServletException {
        Object principal = authResult.getPrincipal();
        if (!(principal instanceof User)) {
            throw new AuthException("User isn`t authenticated");
        }

        User user = (User) principal;

        try {
            String subject = new ObjectMapper().writeValueAsString(toUserModel(user));
            Date expiresAt = new Date(System.currentTimeMillis() + 30000000);
            Algorithm algorithm = Algorithm.HMAC512(secret);
            String token = JWT.create().withSubject(subject).withExpiresAt(expiresAt).sign(algorithm);

            response.addHeader(HttpHeaders.ACCESS_CONTROL_EXPOSE_HEADERS, HttpHeaders.AUTHORIZATION);
            response.addHeader(HttpHeaders.AUTHORIZATION, token);
            response.getWriter().write("Authentication successful");

        } catch (JsonProcessingException e) {
            throw new AuthException(e.getMessage(), e);
        }
    }

    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response,
                                              AuthenticationException failed) throws IOException, ServletException {
        super.unsuccessfulAuthentication(request, response, failed);
    }

    private UserModel toUserModel(User user) {
        UserModel result = new UserModel();
        result.setEmail(user.getUsername());
        List<UserRoleModel> roles = user.getAuthorities()
                                        .stream()
                                        .map(authority -> {
                                            UserRoleModel userRoleModel = new UserRoleModel();
                                            userRoleModel.setRole(UserRole.defineRole(authority.getAuthority()));
                                            return userRoleModel;
                                        }).collect(Collectors.toList());
        result.setRoles(roles);
        return result;
    }
}
