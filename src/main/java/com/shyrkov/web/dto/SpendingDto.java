package com.shyrkov.web.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SpendingDto {

    private Long id;
    private String date;
    private Double sum;
    private CurrencyDto currency;
    private String description;
    private CategoryDto categoryDto;
    private CarInfoDto carInfoDto;

}
