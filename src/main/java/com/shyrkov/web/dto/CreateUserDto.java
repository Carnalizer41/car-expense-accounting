package com.shyrkov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CreateUserDto {

    private Long id;
    private String name;
    private String email;
    private String phone;
    private String currency;
    private String password;
}
