package com.shyrkov.web.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RepairedStatisticsDto {

    private String name;
    private String amount;
    private String averageCost;
}
