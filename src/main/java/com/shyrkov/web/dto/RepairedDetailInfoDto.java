package com.shyrkov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RepairedDetailInfoDto {

    private String name;
    private String serialNumber;

}
