package com.shyrkov.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class CarDto {

    private Long id;
    @JsonProperty("vin_code")
    private String vinCode;
    private String mark;
    private String model;
    private String color;
    private UserDto userEntity;
}
