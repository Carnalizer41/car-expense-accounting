package com.shyrkov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RefuelDto {

    private Long id;
    private Double amount;
    private SpendingDto spendingDto;

}
