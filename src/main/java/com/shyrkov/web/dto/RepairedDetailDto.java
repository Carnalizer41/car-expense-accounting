package com.shyrkov.web.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class RepairedDetailDto {

    private Long id;
    private String name;
    private String serialNumber;
    private SpendingDto spendingDto;


}
