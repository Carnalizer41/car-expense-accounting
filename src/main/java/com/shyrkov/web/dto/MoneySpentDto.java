package com.shyrkov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MoneySpentDto {

    private Double moneySpent;
}
