package com.shyrkov.web.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CrashStatisticsDto {

    private Integer crashesPerYear;
    private Integer crashesPerMonth;
    private String repairCostInYear;
    private String repairCostInMonth;
}
