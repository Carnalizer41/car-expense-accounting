package com.shyrkov.web.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class CarInfoDto {

    private Long id;
    @JsonProperty("vin_code")
    private String vinCode;
    private String mark;
    private String model;
    private String color;
}
