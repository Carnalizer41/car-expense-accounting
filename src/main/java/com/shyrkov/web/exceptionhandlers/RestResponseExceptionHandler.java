package com.shyrkov.web.exceptionhandlers;

import com.shyrkov.exceptions.*;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import static org.springframework.http.HttpStatus.*;

@ControllerAdvice
public class RestResponseExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(value = ValidationException.class)
    protected ResponseEntity<Object> handleValidationException(ValidationException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = NotUniqueEntityException.class)
    protected ResponseEntity<Object> handleNotUniqueEntityException(NotUniqueEntityException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = DateFormerException.class)
    protected ResponseEntity<Object> handleDateFormerException(DateFormerException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = EntityNotFoundException.class)
    protected ResponseEntity<Object> handleEntityNotFoundException(EntityNotFoundException ex, WebRequest request) {
        return handleException(ex, request, BAD_REQUEST);
    }

    @ExceptionHandler(value = ParserException.class)
    protected ResponseEntity<Object> handleParserException(ParserException ex, WebRequest request) {
        return handleException(ex, request, INTERNAL_SERVER_ERROR);
    }

    private ResponseEntity<Object> handleException(RuntimeException ex, WebRequest request, HttpStatus statusCode) {
        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), statusCode, request);
    }
}
