package com.shyrkov.web.controllers;

import com.shyrkov.service.SpendingService;
import com.shyrkov.web.dto.MoneySpentDto;
import com.shyrkov.web.dto.SpendingDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SpendingController {

    private final SpendingService spendingService;

    public SpendingController(SpendingService spendingService) {
        this.spendingService = spendingService;
    }

    @GetMapping(value = "/spending/sum_by_category",
            params = {"carId", "category", "currency"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public MoneySpentDto getMoneySpentByCategory(@RequestParam("carId") Long carId,
                                                 @RequestParam("category") String category,
                                                 @RequestParam("currency") String currency) {
        return spendingService.getMoneySpentOnCarByCategory(carId, category, currency);
    }

    @GetMapping(value = "/spending/history_by_category",
            params = {"category", "car_id"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<SpendingDto> getSpendingHistoryByCategory(@RequestParam("car_id") Long carId,
                                                          @RequestParam("category") String category) {
        return spendingService.getHistoryByCategory(category, carId);
    }
}
