package com.shyrkov.web.controllers;

import com.shyrkov.service.CurrencyService;
import com.shyrkov.web.dto.CurrencyDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/currency")
public class CurrencyController {

    private final CurrencyService currencyService;

    public CurrencyController(CurrencyService currencyService) {
        this.currencyService = currencyService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public CurrencyDto addNewCurrency(@RequestBody CurrencyDto currencyDto){
        return currencyService.addNewCurrency(currencyDto);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public List<CurrencyDto> getAllCurrencies(){
        return currencyService.getAllCurrencies();
    }
}
