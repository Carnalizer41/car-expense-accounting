package com.shyrkov.web.controllers;

import com.shyrkov.service.RefuelService;
import com.shyrkov.web.dto.RefuelAmountDto;
import com.shyrkov.web.dto.RefuelDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

@RestController
public class RefuelController {

    private final RefuelService refuelService;

    public RefuelController(RefuelService refuelService) {
        this.refuelService = refuelService;
    }

    @PostMapping(value = "/refuels", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RefuelDto addNewRefuel(@RequestBody RefuelDto refuelDto){
        return refuelService.addNewRefuel(refuelDto);
    }

    @GetMapping(value = "/refuels/by_car",
                params = {"car_id", "start_date", "end_date"},
                produces = MediaType.APPLICATION_JSON_VALUE)
    public RefuelAmountDto getRefuelAmountByCar(@RequestParam("car_id") Long carId,
                                                 @RequestParam("start_date") String startDate,
                                                 @RequestParam("end_date") String endDate){
        return refuelService.getRefuelAmountByCar(carId, startDate, endDate);
    }

    @GetMapping(value = "/refuels/by_user",
            params = {"start_date", "end_date"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public RefuelAmountDto getRefuelAmountByUser(@RequestParam("start_date") String startDate,
                                                 @RequestParam("end_date") String endDate){
        return refuelService.getRefuelAmountByUser(startDate, endDate);
    }
}
