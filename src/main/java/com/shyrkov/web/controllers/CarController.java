package com.shyrkov.web.controllers;

import com.shyrkov.persistence.entities.CarEntity;
import com.shyrkov.service.CarService;
import com.shyrkov.service.RepairedDetailsService;
import com.shyrkov.service.model.CarsPopularity;
import com.shyrkov.service.model.CarsPopularityImpl;
import com.shyrkov.service.model.CarsReliability;
import com.shyrkov.web.dto.*;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
public class CarController {

    private final CarService carService;
    private final RepairedDetailsService service;

    Double[] daewoo = { 0.243, 0.464, 0.622, 0.745, 0.857, 0.908, 0.951, 0.985,
            1.0 };
    String[] daewooNames = { "Suspension", "Brakes", "Engine", "Lights",
            "Transmission", "Battery", "Wheels", "Gasoline pump", "Computer" };
    Double[] kia = { 0.133, 0.314, 0.42, 0.476, 0.744, 0.805, 0.894, 0.985,
            1.0 };
    String[] kiaNames = { "Transmission", "Brakes", "Suspension", "Engine",
            "Gasoline pump", "Wheels", "Battery", "Lights", "Computer" };
    Double[] hyu = { 0.157, 0.405, 0.529, 0.701, 0.809, 0.853, 0.941, 0.988,
            1.0 };
    String[] hyuNames = { "Brakes", "Lights", "Suspension", "Engine",
            "Transmission", "Wheels", "Gasoline pump", "Battery", "Computer" };
    Double[] ren = { 0.88, 0.223, 0.44, 0.627, 0.769, 0.842, 0.874, 0.907,
            1.0 };
    String[] renNames = { "Engine", "Transmission", "Lights", "Brakes",
            "Computer", "Suspension", "Battery", "Gasoline pump", "Wheels" };
    Double[] volk = { 0.54, 0.287, 0.33, 0.584, 0.748, 0.81, 0.888, 0.985,
            1.0 };
    String[] volkNames = { "Lights", "Brakes", "Transmission", "Gasoline pump",
            "Wheels", "Battery", "Suspension", "Engine", "Computer" };
    Double[] toy = { 0.135, 0.233, 0.383, 0.566, 0.686, 0.79, 0.886, 0.964,
            1.0 };
    String[] toyNames = { "Lights", "Engine", "Suspension", "Transmission",
            "Battery", "Brakes", "Wheels", "Gasoline pump", "Computer" };
    Double[] skoda = { 0.212, 0.28, 0.429, 0.494, 0.661, 0.704, 0.834, 0.955,
            1.0 };
    String[] skodaNames = { "Suspension", "Transmission", "Engine", "Wheels",
            "Gasoline pump", "Brakes", "Lights", "Computer", "Battery" };
    Double[] niss = { 0.154, 0.223, 0.308, 0.521, 0.622, 0.702, 0.893, 0.965,
            1.0 };
    String[] nissNames = { "Lights", "Wheels", "Suspension", "Transmission",
            "Engine", "Battery", "Gasoline pump", "Brakes", "Computer" };
    Double[] audi = { 0.168, 0.263, 0.398, 0.601, 0.679, 0.815, 0.869, 0.931,
            1.0 };
    String[] audiNames = { "Lights", "Suspension", "Battery", "Engine",
            "Brakes", "Transmission", "Computer", "Gasoline pump", "Wheels" };
    Double[] bmw = { 0.134, 0.195, 0.363, 0.554, 0.679, 0.732, 0.842, 0.933,
            1.0 };
    String[] bmwNames = { "Lights", "Engine", "Transmission", "Suspension",
            "Wheels", "Gasoline pump", "Brakes", "Computer", "Battery" };

    public CarController(CarService carService,
            RepairedDetailsService service) {
        this.carService = carService;
        this.service = service;
    }

    @PostMapping(value = "/cars", consumes = APPLICATION_JSON_VALUE)
    public CarDto addNewCar(@RequestBody CarDto carDto) {
        return carService.addNewCar(carDto);
    }

    @PostMapping(value = "/cars/delete_one", consumes = APPLICATION_JSON_VALUE)
    public CarInfoDto removeCar(@RequestBody CarDto carDto) {
        return carService.removeCar(carDto);
    }

    @PostMapping(value = "/cars/edit", consumes = APPLICATION_JSON_VALUE)
    public CarInfoDto editCarInfo(@RequestBody CarDto carDto) {
        return carService.editCarInfo(carDto);
    }

    @GetMapping(value = "/cars", produces = APPLICATION_JSON_VALUE)
    public List<CarInfoDto> getUsersCars() {
        String start = LocalDateTime.now().toString();
        System.out.println();
        Random random = new Random();
        Map<String, Object[]> map = new HashMap<>();
        map.put("Daewoo", new Object[] { daewoo, daewooNames });
        map.put("Kia", new Object[] { kia, kiaNames });
        map.put("Hyundai", new Object[] { hyu, hyuNames });
        map.put("Renault", new Object[] { ren, renNames });
        map.put("Volkswagen", new Object[] { volk, volkNames });
        map.put("Toyota", new Object[] { toy, toyNames });
        map.put("Skoda", new Object[] { skoda, skodaNames });
        map.put("Nissan", new Object[] { niss, nissNames });
        map.put("Audi", new Object[] { audi, audiNames });
        map.put("BMW", new Object[] { bmw, bmwNames });

        //cars
        String[][] marks = {
                { "Daewoo", "Lanos", "Espero", "Magnus", "Lanos", "Chairman",
                        "Kalos", "Gentra" },
                { "Kia", "Ceed", "Sorento", "Rio", "Sonet", "Sportage",
                        "Stinger", "Soul" },
                { "Hyundai", "Accent", "Santro", "Celesta", "Xcent", "Tucson",
                        "Palisade", "ix35" },
                { "Renault", "Logan", "Sandero", "Symbol", "Captur", "Duster",
                        "Kiger", "Koleos" },
                { "Volkswagen", "Golf", "Santana", "Polo", "Arteon", "Jetta",
                        "Passat", "Taos" },
                { "Toyota", "Avalon", "Corolla", "Etios", "Prius", "Aygo",
                        "Passo", "Aqua" },
                { "Skoda", "Octavia", "Scout", "Rapid", "Fabia", "Yeti",
                        "Superb", "Roomster" },
                { "Nissan", "Leaf", "Note", "Tiida", "Sentra", "Juke",
                        "Qashqai", "Rogue" },
                { "Audi", "Q2", "Q7", "TT", "A3", "A7", "A5", "R8" },
                { "BMW", "M3", "M5", "X6", "X7", "X3", "Z4", "X5" } };
        for (int i = 0; i < 2000; i++) {
            CarDto car = new CarDto();
            car.setColor("Red");
            car.setVinCode(String.valueOf(random.nextInt(10000 - 1000) + 1000));

            double koef = Math.random();
            if (koef <= 0.266) {
                car.setMark(marks[0][0]);
                car.setModel(marks[0][random.nextInt(8 - 1) + 1]);
            } else if (koef <= 0.426) {
                car.setMark(marks[1][0]);
                car.setModel(marks[1][random.nextInt(8 - 1) + 1]);
            } else if (koef <= 0.552) {
                car.setMark(marks[2][0]);
                car.setModel(marks[2][random.nextInt(8 - 1) + 1]);
            } else if (koef <= 0.65) {
                car.setMark(marks[3][0]);
                car.setModel(marks[3][random.nextInt(8 - 1) + 1]);
            } else if (koef <= 0.734) {
                car.setMark(marks[4][0]);
                car.setModel(marks[4][random.nextInt(8 - 1) + 1]);
            } else if (koef <= 0.811) {
                car.setMark(marks[5][0]);
                car.setModel(marks[5][random.nextInt(8 - 1) + 1]);
            } else if (koef <= 0.885) {
                car.setMark(marks[6][0]);
                car.setModel(marks[6][random.nextInt(8 - 1) + 1]);
            } else if (koef <= 0.93) {
                car.setMark(marks[7][0]);
                car.setModel(marks[7][random.nextInt(8 - 1) + 1]);
            } else if (koef <= 0.97) {
                car.setMark(marks[8][0]);
                car.setModel(marks[8][random.nextInt(8 - 1) + 1]);
            } else {
                car.setMark(marks[9][0]);
                car.setModel(marks[9][random.nextInt(8 - 1) + 1]);
            }
            UserDto userDto = new UserDto();
            userDto.setId(1L);
            car.setUserEntity(userDto);
            try {
                carService.addNewCar(car);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }

        //repairs
        String[] dates = { "2021-07-13", "2021-08-17", "2021-09-26",
                "2021-09-24", "2021-09-04", "2021-10-08", "2021-10-18",
                "2021-08-29", "2021-08-10" };
        for (int i = 0; i < 50000; i++) {
            RepairedDetailDto dto = new RepairedDetailDto();

            dto.setSerialNumber(
                    String.valueOf(random.nextInt(10000 - 1000) + 1000));
            SpendingDto spendingDto = new SpendingDto();
            spendingDto.setCurrency(
                    new CurrencyDto(1L, "Ukranian Hryvna", "UAH"));
            spendingDto.setCategoryDto(new CategoryDto(1L, "REPAIR"));
            spendingDto.setDescription("qwerty");
            spendingDto.setDate(dates[random.nextInt(9)]);
            CarInfoDto carInfoDto = new CarInfoDto();
            carInfoDto.setId((long) (random.nextInt(299 - 2) + 2));
            CarEntity byId = carService.findById(carInfoDto.getId());
            spendingDto.setCarInfoDto(carInfoDto);
            dto.setSpendingDto(spendingDto);
            qwerty(dto, (Double[]) map.get(byId.getMark())[0],
                    (String[]) map.get(byId.getMark())[1]);
            try {
                service.addNewDetail(dto);
            } catch (Exception e) {
                System.out.println(e.getMessage());
            }
        }
        String end = LocalDateTime.now().toString();
        System.out.println(start + " ======= " + end);
        return carService.getUsersCars();
    }

    private void qwerty(RepairedDetailDto dto, Double[] koefs,
            String[] detailNames) {
        double koef = Math.random();

        if (koef <= koefs[0]) {
            dto.setName(detailNames[0]);
            dto.getSpendingDto().setSum((double) ytrewq(detailNames[0]));
        } else if (koef <= koefs[1]) {
            dto.setName(detailNames[1]);
            dto.getSpendingDto().setSum((double) ytrewq(detailNames[1]));
        } else if (koef <= koefs[2]) {
            dto.setName(detailNames[2]);
            dto.getSpendingDto().setSum((double) ytrewq(detailNames[2]));
        } else if (koef <= koefs[3]) {
            dto.setName(detailNames[3]);
            dto.getSpendingDto().setSum((double) ytrewq(detailNames[3]));
        } else if (koef <= koefs[4]) {
            dto.setName(detailNames[4]);
            dto.getSpendingDto().setSum((double) ytrewq(detailNames[4]));
        } else if (koef <= koefs[5]) {
            dto.setName(detailNames[5]);
            dto.getSpendingDto().setSum((double) ytrewq(detailNames[5]));
        } else if (koef <= koefs[6]) {
            dto.setName(detailNames[6]);
            dto.getSpendingDto().setSum((double) ytrewq(detailNames[6]));
        } else if (koef <= koefs[7]) {
            dto.setName(detailNames[7]);
            dto.getSpendingDto().setSum((double) ytrewq(detailNames[7]));
        } else if (koef <= koefs[8]) {
            dto.setName(detailNames[8]);
            dto.getSpendingDto().setSum((double) ytrewq(detailNames[8]));
        }
    }

    private int ytrewq(String detail) {
        Random random = new Random();
        switch (detail) {
        case "Suspension":
        case "Brakes":
        case "Battery":
            return random.nextInt(10000 - 1000) + 1000;
        case "Engine":
            return random.nextInt(30000 - 10000) + 10000;
        case "Lights":
        case "Wheels":
            return random.nextInt(3000 - 500) + 500;
        case "Transmission":
            return random.nextInt(5000 - 1000) + 1000;
        case "Gasoline pump":
            return random.nextInt(2000 - 500) + 500;
        case "Computer":
            return random.nextInt(8000 - 1500) + 1500;
        }
        return 0;
    }

    @GetMapping(value = "/cars/rating", produces = APPLICATION_JSON_VALUE)
    public Map<String, CrashStatisticsDto> getCarRating() {
        return carService.getCarRating();
    }

    @GetMapping(value = "/cars/popularity", produces = APPLICATION_JSON_VALUE)
    public List<CarsPopularityImpl> getCarPopularity() {
        return carService.getCarPopularity();
    }

    @GetMapping(value = "/cars/mark-popularity", params = {
            "mark" }, produces = APPLICATION_JSON_VALUE)
    public List<CarsPopularityImpl> getCarModelPopularity(
            @RequestParam("mark") String mark) {
        return carService.getCarModelPopularity(mark);
    }

    @GetMapping(value = "/cars/reliability", produces = APPLICATION_JSON_VALUE)
    public List<CarsReliability> getCarReliability() {
        return carService.getCarReliability();
    }

    @GetMapping(value = "/cars/model-rating", produces = APPLICATION_JSON_VALUE)
    public Map<String, CrashStatisticsDto> getCarModelRating() {
        return carService.getCarModelRating();
    }
}
