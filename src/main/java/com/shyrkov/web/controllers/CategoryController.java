package com.shyrkov.web.controllers;

import com.shyrkov.service.CategoryService;
import com.shyrkov.web.dto.CategoryDto;
import com.shyrkov.web.dto.UpdateCategoryDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/category")
public class CategoryController {

    private CategoryService categoryService;

    public CategoryController(CategoryService categoryService) {
        this.categoryService = categoryService;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public CategoryDto addNewCategory(@RequestBody CategoryDto categoryDto){
        return categoryService.addNewCategory(categoryDto);
    }

    @PutMapping(consumes = APPLICATION_JSON_VALUE)
    public CategoryDto updateCategory(@RequestBody UpdateCategoryDto categoryDto){
        return categoryService.updateCategory(categoryDto);
    }

    @GetMapping(produces = APPLICATION_JSON_VALUE)
    public List<CategoryDto> getAllCategories(){
        return categoryService.getAllCategories();
    }
}
