package com.shyrkov.web.controllers;

import com.shyrkov.service.UserService;
import com.shyrkov.web.dto.CreateUserDto;
import com.shyrkov.web.dto.UserDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;


@RestController
public class UserController {

    private final UserService userService;

    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(value = "/users", consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDto createUser(@RequestBody CreateUserDto createUserDto){
        return userService.saveUser(createUserDto);
    }

    @PutMapping(value = "/users/edit_info", consumes = MediaType.APPLICATION_JSON_VALUE)
    public UserDto editUserInfo(@RequestBody CreateUserDto createUserDto){
        return userService.editUserInfo(createUserDto);
    }

    @GetMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
    public UserDto getUserInfo(){
        return userService.getUserInfo();
    }

}
