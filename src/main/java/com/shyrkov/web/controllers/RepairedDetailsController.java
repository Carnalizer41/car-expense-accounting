package com.shyrkov.web.controllers;

import com.shyrkov.service.RepairedDetailsService;
import com.shyrkov.web.dto.MoneySpentDto;
import com.shyrkov.web.dto.RepairedDetailDto;
import com.shyrkov.web.dto.RepairedDetailInfoDto;
import com.shyrkov.web.dto.RepairedStatisticsDto;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
public class RepairedDetailsController {

    private final RepairedDetailsService detailsService;

    public RepairedDetailsController(RepairedDetailsService detailsService) {
        this.detailsService = detailsService;
    }

    @PostMapping(value = "/repaired_details", consumes = MediaType.APPLICATION_JSON_VALUE)
    public RepairedDetailDto addNewRepairedDetail(@RequestBody RepairedDetailDto repairedDetailDto){
        return detailsService.addNewDetail(repairedDetailDto);
    }

    @GetMapping(value = "/repaired_details/statistics",
            params = {"mark"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RepairedStatisticsDto> getRepairedDetailsStatistics(@RequestParam("mark") String mark){
        return detailsService.getRepairedDetailsStatistics(mark);
    }

    @GetMapping(value = "/repaired_details/by_car",
            params = {"car_id", "year",  "currency"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public MoneySpentDto getYearMoneySpentByCar(@RequestParam("car_id") Long carId,
                                                @RequestParam("year") Integer year,
                                                @RequestParam("currency") String currency){
        return detailsService.getYearMoneySpentByCar(carId, year, currency);
    }

    @GetMapping(value = "/repaired_details/by_user",
            params = {"year", "currency"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public MoneySpentDto getYearMoneySpentByUser(@RequestParam("year") Integer year,
                                                 @RequestParam("currency") String currency){
        return detailsService.getYearMoneySpentByUser(year, currency);
    }

    @GetMapping(value = "/repaired_details/list/by_car",
            params = {"car_id"},
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<RepairedDetailInfoDto> getRepairedDetailsListForCar(@RequestParam("car_id") Long carId){
        return detailsService.getRepairedDetailsListForCar(carId);
    }
}
