package com.shyrkov.exceptions;

public class NotUniqueEntityException extends RuntimeException {

    public NotUniqueEntityException(String message) {
        super(message);
    }

    public NotUniqueEntityException(String message, Throwable cause) {
        super(message, cause);
    }
}
