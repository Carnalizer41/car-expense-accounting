package com.shyrkov.persistence.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "spending")
public class SpendingEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "date")
    private String date;
    @Column(name = "sum")
    private Double sum;
    @Column(name = "description")
    private String description;

    @ManyToOne
    @JoinColumn(name = "car_id", referencedColumnName = "id")
    private CarEntity carEntity;

    @ManyToOne
    @JoinColumn(name = "category_id", referencedColumnName = "id")
    private CategoryEntity categoryEntity;

    @OneToMany(mappedBy = "detailsSpendingEntity")
    private List<RepairedDetailsEntity> repairedDetailsEntities = new ArrayList<>();

    @OneToMany(mappedBy = "refuelSpendingEntity")
    private List<RefuelEntity> refuelEntities = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "currency_id", referencedColumnName = "id")
    private CurrencyEntity currencyEntity;
}
