package com.shyrkov.persistence.entities;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Getter
@Setter
@Entity
@Table(name = "refuels")
public class RefuelEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Column(name = "amount")
    private Double amount;

    @ManyToOne
    @JoinColumn(name = "spending_id", referencedColumnName = "id")
    private SpendingEntity refuelSpendingEntity;
}
