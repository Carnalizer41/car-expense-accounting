package com.shyrkov.persistence;

import com.shyrkov.persistence.entities.SpendingEntity;
import com.shyrkov.service.model.MoneyByCurrency;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SpendingDao extends JpaRepository<SpendingEntity, Long> {

    List<SpendingEntity> findAll();

    @Query("select s.currencyEntity.code as currency, sum(s.sum) as sum " +
            "from SpendingEntity s " +
            "where s.categoryEntity.id = :categoryId and " +
            "s.carEntity.id = :carId " +
            "group by currency")
    List<MoneyByCurrency> getMoneyByCategoryAndCar(@Param("categoryId") Long categoryId,
                                                   @Param("carId") Long carId);

    List<SpendingEntity> findByCategoryEntityIdAndCarEntityId(Long categoryId, Long carId);
}
