package com.shyrkov.persistence;

import com.shyrkov.persistence.entities.RepairedDetailsEntity;
import com.shyrkov.service.model.MoneyByCurrency;
import com.shyrkov.service.model.RepairedDetailStatistics;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface RepairedDetailsDao extends JpaRepository<RepairedDetailsEntity, Long> {

    @Query("select d.name as name, count(d.id) as amount, sum(d.detailsSpendingEntity.sum) as cost " +
            "from RepairedDetailsEntity d " +
            "where d.detailsSpendingEntity.carEntity.mark = :mark " +
            "group by d.name")
    List<RepairedDetailStatistics> getDetailsStatistics(@Param("mark") String mark);

    @Query("select d.detailsSpendingEntity.currencyEntity.code as currency, sum(d.detailsSpendingEntity.sum) as sum " +
            "from RepairedDetailsEntity d " +
            "where d.detailsSpendingEntity.carEntity.id = :carId " +
            "and d.detailsSpendingEntity.date between :startDate and :endDate " +
            "group by currency")
    List<MoneyByCurrency> getMoneySpentByCar(@Param("carId") Long carId,
                                             @Param("startDate") String startDate,
                                             @Param("endDate") String endDate);

    @Query("select d.detailsSpendingEntity.currencyEntity.code as currency, sum(d.detailsSpendingEntity.sum) as sum " +
            "from RepairedDetailsEntity d " +
            "where d.detailsSpendingEntity.carEntity.userEntity.id = :userId " +
            "and d.detailsSpendingEntity.date between :startDate and :endDate " +
            "group by currency")
    List<MoneyByCurrency> getMoneySpentByUser(@Param("userId") Long userId,
                                              @Param("startDate") String startDate,
                                              @Param("endDate") String endDate);

    @Query("select d from RepairedDetailsEntity d " +
            "where d.detailsSpendingEntity.carEntity.id = :carId ")
    List<RepairedDetailsEntity> findByCarId(@Param("carId") Long carId);

    @Query("select count(d.id) from RepairedDetailsEntity d " +
            "where d.detailsSpendingEntity.carEntity.mark = :mark ")
    int getRepairsAmountByMark(@Param("mark") String mark);

    @Query(nativeQuery = true,
            value = "select exists (select * from repaired_details where serial_number = :serialNumber)")
    boolean existsBySerialNumber(@Param("serialNumber") String serialNumber);
}
