package com.shyrkov.persistence;

import com.shyrkov.persistence.entities.RefuelEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface RefuelDao extends JpaRepository<RefuelEntity, Long> {

    @Query("select sum(r.amount) from RefuelEntity r " +
            "where r.refuelSpendingEntity.carEntity.id = :carId " +
            "and r.refuelSpendingEntity.date between :startDate and :endDate")
    Double findAmountByCar(@Param("carId") Long carId,
                           @Param("startDate") String startDate,
                           @Param("endDate") String endDate);

    @Query("select sum(r.amount) from RefuelEntity r " +
            "where r.refuelSpendingEntity.carEntity.userEntity.id = :userId " +
            "and r.refuelSpendingEntity.date between :startDate and :endDate")
    Double findAmountByUser(@Param("userId") Long userId,
                           @Param("startDate") String startDate,
                           @Param("endDate") String endDate);

}
