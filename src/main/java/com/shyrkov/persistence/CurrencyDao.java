package com.shyrkov.persistence;

import com.shyrkov.persistence.entities.CurrencyEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface CurrencyDao extends JpaRepository<CurrencyEntity, Long> {

    CurrencyEntity findOneByCode(String code);

    CurrencyEntity findOneById(Long id);

    List<CurrencyEntity> findAll();
}
