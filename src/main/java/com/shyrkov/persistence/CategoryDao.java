package com.shyrkov.persistence;

import com.shyrkov.persistence.entities.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CategoryDao extends JpaRepository<CategoryEntity, Long> {

    List<CategoryEntity> findAll();

    CategoryEntity findOneByName(String name);

    @Transactional
    @Modifying
    @Query("update CategoryEntity as c " +
            "set c.name = :name " +
            "where c.id = :id")
    int updateCategoryInfo(@Param("id") Long id,
                           @Param("name") String name);

    @Query(nativeQuery = true,
            value = "select exists (select * from category where name = :name)")
    boolean existsByExpenseType(@Param("name") String name);

}
