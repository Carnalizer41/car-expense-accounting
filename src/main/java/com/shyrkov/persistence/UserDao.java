package com.shyrkov.persistence;

import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.persistence.entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface UserDao extends JpaRepository<UserEntity, Long> {

    UserEntity findOneByEmail(String email);

    List<UserEntity> findAll();

    @Transactional
    @Modifying
    @Query("update UserEntity as u set " +
            "u.name = :name, u.email = :email, " +
            "u.phone = :phone, u.currency = :currency, " +
            "u.password = :password " +
            "where u.id = :user_id")
    Integer updateUserInfo(@Param("name") String name, @Param("email") String email,
                           @Param("phone") String phone, @Param("currency") CurrencyEntity currency,
                           @Param("user_id") Long user_id, @Param("password") String password);

    @Query(nativeQuery = true,
            value = "select exists (select * from users where email = :email)")
    boolean existsByEmail(@Param("email") String email);

}
