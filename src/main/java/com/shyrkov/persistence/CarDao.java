package com.shyrkov.persistence;

import com.shyrkov.persistence.entities.CarEntity;
import com.shyrkov.service.model.CarsPopularity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface CarDao extends JpaRepository<CarEntity, Long> {

    @Query("select c from CarEntity c " +
            "where c.userEntity.id = :userId ")
    List<CarEntity> findByUser(@Param("userId") Long userId);

    @Query("select count(c) from CarEntity c " +
            "where c.mark = :mark")
    int getAmountByMark(@Param("mark") String mark);

    @Query("select c.mark as name, count(c.id) as amount " +
            "from CarEntity c " +
            "group by c.mark")
    List<CarsPopularity> getCarsPopularity();

    @Query("select c.model as name, count(c.id) as amount " +
            "from CarEntity c " +
            "where c.mark = :mark " +
            "group by c.model")
    List<CarsPopularity> getCarsModelPopularity(@Param("mark") String mark);

    @Transactional
    @Modifying
    @Query("update CarEntity as c set " +
            "c.vinCode = :vin, c.mark = :mark, " +
            "c.model = :model, c.color = :color " +
            "where c.id = :carId and c.userEntity.id = :userId")
    int editCarInfo(@Param("userId") Long userId, @Param("carId") Long carId, @Param("vin") String vin,
                    @Param("mark") String mark, @Param("model") String model, @Param("color") String color);

    @Query(nativeQuery = true,
            value = "select exists (select * from cars where vin_code = :vinCode)")
    boolean existsByVinCode(@Param("vinCode") String vinCode);

    CarEntity findOneByVinCode(String vinCode);

    @Transactional
    int deleteOneById(Long id);

    CarEntity findOneById(Long id);

    List<CarEntity> findAll();

}
