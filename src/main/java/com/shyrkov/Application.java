package com.shyrkov;

import com.shyrkov.persistence.entities.CarEntity;
import com.shyrkov.service.CarService;
import com.shyrkov.service.RepairedDetailsService;
import com.shyrkov.web.dto.*;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

@SpringBootApplication
public class Application {

//    private static void qwerty(RepairedDetailDto dto, Double[] koefs,
//            String[] detailNames) {
//        double koef = Math.random();
//
//        if (koef <= koefs[0]) {
//            dto.setName(detailNames[0]);
//        } else if (koef <= koefs[1]) {
//            dto.setName(detailNames[1]);
//        } else if (koef <= koefs[2]) {
//            dto.setName(detailNames[2]);
//        } else if (koef <= koefs[3]) {
//            dto.setName(detailNames[3]);
//        } else if (koef <= koefs[4]) {
//            dto.setName(detailNames[4]);
//        } else if (koef <= koefs[5]) {
//            dto.setName(detailNames[5]);
//        } else if (koef <= koefs[6]) {
//            dto.setName(detailNames[6]);
//        } else if (koef <= koefs[7]) {
//            dto.setName(detailNames[7]);
//        } else if (koef <= koefs[8]) {
//            dto.setName(detailNames[8]);
//        }
//    }

    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);

//        Double[] daewoo = { 0.243, 0.464, 0.622, 0.745, 0.857, 0.908, 0.951, 0.985,
//                1.0 };
//        String[] daewooNames = { "Suspension", "Brakes", "Engine", "Lights",
//                "Transmission", "Battery", "Wheels", "Gasoline pump", "Computer" };
//        Double[] kia = { 0.133, 0.314, 0.42, 0.476, 0.744, 0.805, 0.894, 0.985,
//                1.0 };
//        String[] kiaNames = { "Transmission", "Brakes", "Suspension", "Engine",
//                "Gasoline pump", "Wheels", "Battery", "Lights", "Computer" };
//        Double[] hyu = { 0.157, 0.405, 0.529, 0.701, 0.809, 0.853, 0.941, 0.988,
//                1.0 };
//        String[] hyuNames = { "Brakes", "Lights", "Suspension", "Engine",
//                "Transmission", "Wheels", "Gasoline pump", "Battery", "Computer" };
//        Double[] ren = { 0.88, 0.223, 0.44, 0.627, 0.769, 0.842, 0.874, 0.907,
//                1.0 };
//        String[] renNames = { "Lights", "Engine", "Transmission", "Brakes",
//                "Computer", "Suspension", "Battery", "Gasoline pump", "Wheels" };
//        Double[] volk = { 0.54, 0.287, 0.33, 0.584, 0.748, 0.81, 0.888, 0.985,
//                1.0 };
//        String[] volkNames = { "Lights", "Brakes", "Transmission", "Gasoline pump",
//                "Wheels", "Battery", "Suspension", "Engine", "Computer" };
//        Double[] toy = { 0.135, 0.233, 0.383, 0.566, 0.686, 0.79, 0.886, 0.964,
//                1.0 };
//        String[] toyNames = { "Lights", "Engine", "Suspension", "Transmission",
//                "Battery", "Brakes", "Wheels", "Gasoline pump", "Computer" };
//        Double[] skoda = { 0.212, 0.28, 0.429, 0.494, 0.661, 0.704, 0.834, 0.955,
//                1.0 };
//        String[] skodaNames = { "Suspension", "Transmission", "Engine", "Wheels",
//                "Gasoline pump", "Brakes", "Lights", "Computer", "Battery" };
//        Double[] niss = { 0.154, 0.223, 0.308, 0.521, 0.622, 0.702, 0.893, 0.965,
//                1.0 };
//        String[] nissNames = { "Lights", "Wheels", "Suspension", "Transmission",
//                "Engine", "Battery", "Gasoline pump", "Brakes", "Computer" };
//        Double[] audi = { 0.168, 0.263, 0.398, 0.601, 0.679, 0.815, 0.869, 0.931,
//                1.0 };
//        String[] audiNames = { "Lights", "Suspension", "Battery", "Engine",
//                "Brakes", "Transmission", "Computer", "Gasoline pump", "Wheels" };
//        Double[] bmw = { 0.134, 0.195, 0.363, 0.554, 0.679, 0.732, 0.842, 0.933,
//                1.0 };
//        String[] bmwNames = { "Lights", "Engine", "Transmission", "Suspension",
//                "Wheels", "Gasoline pump", "Brakes", "Computer", "Battery" };
//
//
//        Map<String, Object[]> map = new HashMap<>();
//        map.put("Daewoo", new Object[] { daewoo, daewooNames });
//        map.put("Kia", new Object[] { kia, kiaNames });
//        map.put("Hyundai", new Object[] { hyu, hyuNames });
//        map.put("Renault", new Object[] { ren, renNames });
//        map.put("Volkswagen", new Object[] { volk, volkNames });
//        map.put("Toyota", new Object[] { toy, toyNames });
//        map.put("Skoda", new Object[] { skoda, skodaNames });
//        map.put("Nissan", new Object[] { niss, nissNames });
//        map.put("Audi", new Object[] { audi, audiNames });
//        map.put("BMW", new Object[] { bmw, bmwNames });
//
//        RepairedDetailDto dto = new RepairedDetailDto();
//        CarEntity byId = new CarEntity();
//        byId.setMark("Daewoo");
//        qwerty(dto, (Double[]) map.get(byId.getMark())[0],
//                (String[]) map.get(byId.getMark())[1]);
//        System.out.println(dto.getName());

//        int count1 = 0;
//        int count2 = 0;
//        int count3 = 0;
//        int count4 = 0;
//        int count5 = 0;
//        int count6 = 0;
//        int count7 = 0;
//        int count8 = 0;
//        int count9 = 0;
//        int count10 = 0;
//
//        for (int i = 0; i < 1000; i++) {
//            double koef = Math.random();
//            if(koef>=0 && koef<=0.266){
//                count1++;
//            }else
//            if(koef>0.266 && koef<=0.426){
//                count2++;
//            }else
//            if(koef>0.426 && koef<=0.552){
//                count3++;
//            }else
//            if(koef>0.552 && koef<=0.65){
//                count4++;
//            }else
//            if(koef>0.65 && koef<=0.734){
//                count5++;
//            }else
//            if(koef>0.734 && koef<=0.811){
//                count6++;
//            }else
//            if(koef>0.811 && koef<=0.885){
//                count7++;
//            }else
//            if(koef>0.885 && koef<=0.93){
//                count8++;
//            }else
//            if(koef>0.93 && koef<=0.97){
//                count9++;
//            }else
//            if(koef>0.97){
//                count10++;
//            }
//        }
//
//        System.out.println(count1 + " 1");
//        System.out.println(count2 + " 2");
//        System.out.println(count3 + " 3");
//        System.out.println(count4 + " 4");
//        System.out.println(count5 + " 5");
//        System.out.println(count6 + " 6");
//        System.out.println(count7 + " 7");
//        System.out.println(count8 + " 8");
//        System.out.println(count9 + " 9");
//        System.out.println(count10 + " 10");

//        Random random = new Random();
//        String[][] marks = {
//                { "Daewoo", "Lanos", "Espero", "Magnus", "Lanos", "Chairman",
//                        "Kalos", "Gentra" },
//                { "Kia", "Ceed", "Sorento", "Rio", "Sonet", "Sportage",
//                        "Stinger", "Soul" },
//                { "Hyundai", "Accent", "Santro", "Celesta", "Xcent", "Tucson",
//                        "Palisade", "ix35" },
//                { "Renault", "Logan", "Sandero", "Symbol", "Captur", "Duster",
//                        "Kiger", "Koleos" },
//                { "Volkswagen", "Golf", "Santana", "Polo", "Arteon", "Jetta",
//                        "Passat", "Taos" },
//                { "Toyota", "Avalon", "Corolla", "Etios", "Prius", "Aygo",
//                        "Passo", "Aqua" },
//                { "Skoda", "Octavia", "Scout", "Rapid", "Fabia", "Yeti",
//                        "Superb", "Roomster" },
//                { "Nissan", "Leaf", "Note", "Tiida", "Sentra", "Juke",
//                        "Qashqai", "Rogue" },
//                { "Audi", "Q2", "Q7", "TT", "A3", "A7", "A5", "R8" },
//                { "BMW", "M3", "M5", "X6", "X7", "X3", "Z4", "X5" } };
//        for (int i = 0; i < 300; i++) {
//            CarDto car = new CarDto();
//            car.setColor("Red");
//            car.setVinCode(String.valueOf(random.nextInt(10000 - 1000) + 1000));
//            int mark = random.nextInt(10);
//            car.setMark(marks[mark][0]);
//            car.setModel(marks[mark][random.nextInt(8 - 1) + 1]);
//            UserDto userDto = new UserDto();
//            userDto.setId(1L);
//            car.setUserEntity(userDto);
//            try {
//                service.addNewCar(car);
//            }catch (Exception e){
//                System.out.println(e.getMessage());
//            }
//
//        }
        //        for (int i = 0; i < 50; i++) {
        //            int mark = random.nextInt(10);
        //            System.out.print(
        //                    marks[mark][0]);
        //            System.out.println(" "+
        //                    marks[mark][random.nextInt(8 - 1) + 1]);
        //        }
//        String[] detailNames = { "Brakes", "Wheels", "Engine", "Transmission",
//                "Computer", "Gasoline pump", "Suspension", "Lights", "Battery" };
//                String[] dates = { "2021-07-13", "2021-08-17", "2021-09-26", "2021-09-24",
//                        "2021-09-04", "2021-10-08", "2021-10-18", "2021-08-29", "2021-08-10" };
//                for (int i = 0; i < 10; i++) {
//                    RepairedDetailDto dto = new RepairedDetailDto();
//                    dto.setName(detailNames[random.nextInt(9)]);
//                    dto.setSerialNumber(String.valueOf(random.nextInt(10000)));
//                    SpendingDto spendingDto = new SpendingDto();
//                    spendingDto.setCurrency(
//                            new CurrencyDto(1L, "Ukranian Hryvna", "UAH"));
//                    spendingDto.setCategoryDto(new CategoryDto(1L, "REPAIR"));
//                    spendingDto.setDescription("");
//                    spendingDto.setSum((double) (random.nextInt(50001 - 1000) + 1000));
//                    spendingDto.setDate(dates[random.nextInt(9)]);
//                    CarInfoDto carInfoDto = new CarInfoDto();
//                    carInfoDto.setId((long) (random.nextInt(299 - 2) + 2));
//                    spendingDto.setCarInfoDto(carInfoDto);
//                    dto.setSpendingDto(spendingDto);
//                    System.out.println(dto);
//                }
    }

}
