CREATE TABLE IF NOT EXISTS user_roles
(
    id          SERIAL        PRIMARY KEY,
    role        VARCHAR(50)   NOT NULL UNIQUE,
    user_id     BIGINT        NOT NULL,
    CONSTRAINT "user_id_fkey" FOREIGN KEY (user_id) REFERENCES users (id)
);
