ALTER TABLE users
ADD COLUMN currency_id BIGINT NOT NULL DEFAULT '1',
add constraint "currency_id_fkey" FOREIGN KEY (currency_id) REFERENCES "currency" (id)