INSERT INTO public.users(name, email, password, phone)
VALUES ('admin', 'admin@mail.com', '$2a$10$sA4ts93FKtF7XU1B5qik8OazyHJY9EreEbbsNmixZ.TNmXCgZ0xAK', '+380997777777');

INSERT INTO user_roles(role, user_id)
values ('ADMIN', 1)