CREATE TABLE IF NOT EXISTS currency
(
    id              SERIAL          PRIMARY KEY,
    "name"          VARCHAR(50)     NOT NULL UNIQUE,
    "code"          VARCHAR(5)      NOT NULL UNIQUE
);

INSERT INTO public.currency(name, code)
VALUES ('Ukranian Hryvna', 'UAH');
INSERT INTO public.currency(name, code)
VALUES ('United States Dollar', 'USD');
