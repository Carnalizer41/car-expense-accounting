CREATE TABLE IF NOT EXISTS users
(
    id              SERIAL          PRIMARY KEY,
    "name"          VARCHAR(50)     NOT NULL,
    email           VARCHAR(1024)   NOT NULL UNIQUE,
    password        VARCHAR(256)    NOT NULL,
    phone           VARCHAR(20)     NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS "category"
(
    id              SERIAL          PRIMARY KEY,
    "name"          VARCHAR(256)    NOT NULL UNIQUE
);

CREATE TABLE IF NOT EXISTS cars
(
    id              SERIAL          PRIMARY KEY,
    vin_code         VARCHAR(50)     NOT NULL UNIQUE,
    mark            VARCHAR(50)     NOT NULL,
    model           VARCHAR(50)     NOT NULL,
    color           VARCHAR(50)     NOT null,
    user_id         BIGINT          NOT NULL,
	constraint "user_id_fkey" FOREIGN KEY (user_id) REFERENCES "users" (id)
);

CREATE TABLE IF NOT EXISTS spending
(
    id              SERIAL PRIMARY KEY,
    category_id     BIGINT          NOT NULL,
    car_id          BIGINT          NOT NULL,
    "date"          VARCHAR(50)     NOT NULL,
    sum             NUMERIC(10, 4)  NOT NULL,
    currency        VARCHAR(32)     NOT NULL,
    description     text,
	constraint "category_id_fkey" FOREIGN KEY (category_id) REFERENCES "category" (id),
	constraint "car_id_fkey"      FOREIGN KEY (car_id)      REFERENCES "cars" (id)
);

CREATE TABLE IF NOT EXISTS repaired_details
(
    id              SERIAL          PRIMARY KEY,
    "name"          VARCHAR(256)    NOT NULL,
    serial_number   VARCHAR(256)    NOT NULL UNIQUE,
    spending_id     BIGINT          NOT NULL,
	constraint "spending_id_fkey" FOREIGN KEY (spending_id) REFERENCES "spending" (id)
);

CREATE TABLE IF NOT EXISTS refuels
(
    id              SERIAL          PRIMARY KEY,
    amount          NUMERIC(10, 4)  NOT NULL,
    spending_id     BIGINT          NOT NULL,
	constraint "spending_id_fkey" FOREIGN KEY (spending_id) REFERENCES "spending" (id)
);




