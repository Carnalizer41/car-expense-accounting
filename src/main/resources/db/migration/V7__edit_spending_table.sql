ALTER TABLE spending
drop column currency,
add column currency_id BIGINT NOT NULL,
add constraint "currency_id_fkey" FOREIGN KEY (currency_id) REFERENCES "currency" (id)
