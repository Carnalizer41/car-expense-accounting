package com.shyrkov.service;

import com.shyrkov.exceptions.EntityNotFoundException;
import com.shyrkov.persistence.CurrencyDao;
import com.shyrkov.persistence.UserDao;
import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.service.mappers.CurrencyMapper;
import com.shyrkov.service.mappers.UserMapper;
import com.shyrkov.service.validation.NewUserValidator;
import com.shyrkov.web.dto.CreateUserDto;
import com.shyrkov.web.dto.CurrencyDto;
import com.shyrkov.web.dto.UserDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class UserServiceTest {

    @Captor
    private ArgumentCaptor<UserDto> userDtoCaptor;
    @Captor
    private ArgumentCaptor<UserEntity> userEntityCaptor;
    @Mock
    private UserDao userDao;
    @Mock
    private CurrencyDao currencyDao;
    @Mock
    private UserMapper userMapper;
    @Mock
    private CurrencyMapper currencyMapper;
    @Mock
    private NewUserValidator userValidator;
    @Mock
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    @InjectMocks
    private UserService userService;

    @Test
    public void newUserWillBeSavedToDpAndUserDtoWillBeReturnedWhenGivenNewUserDto() {
        String testEmail = "some_email@gmail.com";
        String testPassword = "qwerty";
        String testPhone = "+380999999999";
        String testName = "test_name";
        Long testId = 1L;
        CurrencyEntity currencyEntity = new CurrencyEntity();
        currencyEntity.setId(1L);
        currencyEntity.setName("Ukrainian Hryvna");
        currencyEntity.setCode("UAH");
        CreateUserDto newUser = new CreateUserDto();
        newUser.setEmail(testEmail);
        newUser.setPassword(testPassword);
        newUser.setPhone(testPhone);
        newUser.setName(testName);
        newUser.setCurrency("UAH");
        UserDto userDto = new UserDto();
        userDto.setId(newUser.getId());
        userDto.setName(newUser.getName());
        userDto.setEmail(newUser.getEmail());
        userDto.setPhone(newUser.getPhone());
        userDto.setCurrency(new CurrencyDto(1L, "Ukrainian Hryvna", "UAH"));
        UserEntity newUserEntity = new UserEntity();
        newUserEntity.setEmail(testEmail);
        newUserEntity.setPassword(testPassword);
        newUserEntity.setPhone(testPhone);
        newUserEntity.setName(testName);
        newUserEntity.setCurrency(currencyEntity);
        UserEntity savedUserEntity = new UserEntity();
        savedUserEntity.setId(testId);
        savedUserEntity.setEmail(testEmail);
        savedUserEntity.setPassword(testPassword);
        savedUserEntity.setPhone(testPhone);
        savedUserEntity.setName(testName);
        CurrencyDto currencyDto = new CurrencyDto(1L, "Ukrainian Hryvna", "UAH");
        UserDto savedUserDto = new UserDto();
        savedUserDto.setEmail(testEmail);
        savedUserDto.setPhone(testPhone);
        savedUserDto.setName(testName);
        savedUserDto.setId(testId);
        savedUserDto.setCurrency(currencyDto);

        doNothing().when(userValidator).validate(newUser);
        doReturn(newUserEntity).when(userMapper).dtoToEntity(userDto);
        doReturn(currencyEntity).when(currencyDao).findOneByCode(newUser.getCurrency());
        doReturn(savedUserEntity).when(userDao).save(newUserEntity);
        doReturn(savedUserDto).when(userMapper).entityToDto(savedUserEntity);
        doReturn(currencyDto).when(currencyMapper).entityToDto(newUserEntity.getCurrency());


        UserDto returnedUser = userService.saveUser(newUser);

        verify(userValidator).validate(newUser);
        verify(userMapper).dtoToEntity(userDtoCaptor.capture());
        verify(userDao).save(userEntityCaptor.capture());
        verify(userMapper).entityToDto(userEntityCaptor.capture());

        assertEquals(testEmail, userDtoCaptor.getValue().getEmail());
        assertEquals(testName, userDtoCaptor.getValue().getName());
        assertEquals(testPhone, userDtoCaptor.getValue().getPhone());
        assertEquals(testEmail, userEntityCaptor.getAllValues().get(0).getEmail());
        assertEquals(testName, userEntityCaptor.getAllValues().get(0).getName());
        assertEquals(testPhone, userEntityCaptor.getAllValues().get(0).getPhone());
        assertNull(userEntityCaptor.getAllValues().get(0).getId());
        assertEquals(1, userEntityCaptor.getAllValues().get(0).getUserRoles().size());
        assertEquals(testEmail, userEntityCaptor.getAllValues().get(1).getEmail());
        assertEquals(testPassword, userEntityCaptor.getAllValues().get(1).getPassword());
        assertEquals(testName, userEntityCaptor.getAllValues().get(1).getName());
        assertEquals(testPhone, userEntityCaptor.getAllValues().get(1).getPhone());
        assertEquals(testId, userEntityCaptor.getAllValues().get(1).getId());
        assertEquals(savedUserDto, returnedUser);
        assertEquals(testEmail, returnedUser.getEmail());
        assertEquals(testName, returnedUser.getName());
        assertEquals(testPhone, returnedUser.getPhone());
        assertEquals(testId, returnedUser.getId());
    }

    @Test
    public void entityNotFoundExceptionWillBeThrownWhenGivenUnknownEmail() {
        String email = "test_email@gmail.com";

        doReturn(null).when(userDao).findOneByEmail(email);

        assertThrows(EntityNotFoundException.class, () -> userService.loadUserByUsername(email));

        verify(userDao).findOneByEmail(email);
        verifyNoInteractions(userMapper);
    }
}