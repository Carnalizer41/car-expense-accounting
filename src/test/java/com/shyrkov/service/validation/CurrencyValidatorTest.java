package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.dto.CurrencyDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class CurrencyValidatorTest {

    private final CurrencyDto currencyDto = new CurrencyDto();
    private final CurrencyValidator validator = new CurrencyValidator();

    @BeforeEach
    public void init() {
        currencyDto.setId(1L);
        currencyDto.setName("Ukrainian Hryvna");
        currencyDto.setCode("UAH");
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenValidCurrency() {
        assertDoesNotThrow(() -> validator.validate(currencyDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNullCode() {
        currencyDto.setCode(null);

        assertThrows(ValidationException.class, () -> validator.validate(currencyDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenBlankCode() {
        currencyDto.setCode("");

        assertThrows(ValidationException.class, () -> validator.validate(currencyDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNullName() {
        currencyDto.setName(null);

        assertThrows(ValidationException.class, () -> validator.validate(currencyDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenBlankName() {
        currencyDto.setName("");

        assertThrows(ValidationException.class, () -> validator.validate(currencyDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCodeHasUnsupportedCharacters() {
        currencyDto.setCode("U$A1H");

        assertThrows(ValidationException.class, () -> validator.validate(currencyDto));
    }
}