package com.shyrkov.service.validation;

import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.persistence.RepairedDetailsDao;
import com.shyrkov.web.dto.RepairedDetailDto;
import com.shyrkov.web.dto.SpendingDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NewRepairedDetailValidatorTest {

    @Mock
    private ExpenseInfoValidator infoValidator;
    @Mock
    private RepairedDetailsDao repairedDetailsDao;
    private final RepairedDetailDto detailDto = new RepairedDetailDto();
    private final SpendingDto spendingDto = new SpendingDto();
    @InjectMocks
    private NewRepairedDetailValidator detailValidator;

    @BeforeEach
    public void init() {
        detailDto.setName("Some spare part");
        detailDto.setSerialNumber("W123");
        detailDto.setSpendingDto(spendingDto);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenNewRepairedDetailDtoWithValidData() {
        doNothing().when(infoValidator).validate(detailDto.getSpendingDto());
        doReturn(false).when(repairedDetailsDao).existsBySerialNumber(detailDto.getSerialNumber());

        assertDoesNotThrow(() -> detailValidator.validate(detailDto));

        verify(infoValidator).validate(detailDto.getSpendingDto());
        verify(repairedDetailsDao).existsBySerialNumber(detailDto.getSerialNumber());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewRepairedDetailDtoWithNullSerialNumber() {
        detailDto.setSerialNumber(null);

        assertThrows(ValidationException.class, () -> detailValidator.validate(detailDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(repairedDetailsDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewRepairedDetailDtoWithBlankSerialNumber() {
        detailDto.setSerialNumber("");

        assertThrows(ValidationException.class, () -> detailValidator.validate(detailDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(repairedDetailsDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewRepairedDetailDtoWithNullName() {
        detailDto.setName(null);

        assertThrows(ValidationException.class, () -> detailValidator.validate(detailDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(repairedDetailsDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewRepairedDetailDtoWithBlankName() {
        detailDto.setName("");

        assertThrows(ValidationException.class, () -> detailValidator.validate(detailDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(repairedDetailsDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewRepairedDetailDtoWithNullExpenseInfo() {
        detailDto.setSpendingDto(null);

        assertThrows(ValidationException.class, () -> detailValidator.validate(detailDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(repairedDetailsDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewRepairedDetailDtoWithTooShortSerialNumber() {
        detailDto.setSerialNumber("123");

        assertThrows(ValidationException.class, () -> detailValidator.validate(detailDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(repairedDetailsDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNewRepairedDetailDtoWithSerialNumberWithUnsupportedCharacters() {
        detailDto.setSerialNumber("W124$");

        assertThrows(ValidationException.class, () -> detailValidator.validate(detailDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(repairedDetailsDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenNewRepairedDetailDtoWithSerialNumberConsistingOfSeveralWords() {
        detailDto.setSerialNumber("W 123");

        doNothing().when(infoValidator).validate(detailDto.getSpendingDto());
        doReturn(false).when(repairedDetailsDao).existsBySerialNumber(detailDto.getSerialNumber());

        assertDoesNotThrow(() -> detailValidator.validate(detailDto));

        verify(infoValidator).validate(detailDto.getSpendingDto());
        verify(repairedDetailsDao).existsBySerialNumber(detailDto.getSerialNumber());
    }

    @Test
    public void notUniqueEntityExceptionWillBeThrownWhenGivenNewRepairedDetailDtoWithNotUniqueSerialNumber() {
        doNothing().when(infoValidator).validate(detailDto.getSpendingDto());
        doReturn(true).when(repairedDetailsDao).existsBySerialNumber(detailDto.getSerialNumber());

        assertThrows(NotUniqueEntityException.class, () -> detailValidator.validate(detailDto));

        verify(infoValidator).validate(detailDto.getSpendingDto());
        verify(repairedDetailsDao).existsBySerialNumber(detailDto.getSerialNumber());
    }
}