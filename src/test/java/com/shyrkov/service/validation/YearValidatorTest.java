package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class YearValidatorTest {

    private final YearValidator yearValidator = new YearValidator();

    @Test
    public void validationWillBeSuccessfulWhenGivenYearWithinRange() {
        assertDoesNotThrow(() -> yearValidator.validate(2020));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenNullYear() {
        assertThrows(ValidationException.class, () -> yearValidator.validate(null));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenYearTooFarFromThePast() {
        assertThrows(ValidationException.class, () -> yearValidator.validate(1899));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenYearTooFarFromTheFuture() {
        assertThrows(ValidationException.class, () -> yearValidator.validate(2101));
    }
}