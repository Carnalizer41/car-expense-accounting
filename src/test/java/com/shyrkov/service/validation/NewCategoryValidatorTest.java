package com.shyrkov.service.validation;

import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.persistence.CategoryDao;
import com.shyrkov.web.dto.CategoryDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NewCategoryValidatorTest {

    @Mock
    private CategoryValidator categoryValidator;
    @Mock
    private CategoryDao categoryDao;
    @InjectMocks
    private NewCategoryValidator newCategoryValidator;
    private final CategoryDto category = new CategoryDto();

    @Test
    public void notUniqueEntityExceptionWillBeThrownWhenGivenCategoryDtoWithNotUniqueName() {
        category.setName("REFUEL");

        doNothing().when(categoryValidator).validate(category);
        doReturn(true).when(categoryDao).existsByExpenseType(category.getName());

        assertThrows(NotUniqueEntityException.class, () -> newCategoryValidator.validate(category));

        verify(categoryValidator).validate(category);
        verify(categoryDao).existsByExpenseType(category.getName());
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCategoryDtoWithValidName() {
        category.setName("PARKING");

        doNothing().when(categoryValidator).validate(category);
        doReturn(false).when(categoryDao).existsByExpenseType(category.getName());

        assertDoesNotThrow(() -> newCategoryValidator.validate(category));

        verify(categoryValidator).validate(category);
        verify(categoryDao).existsByExpenseType(category.getName());
    }
}