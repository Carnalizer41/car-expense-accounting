package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class DateValidatorTest {

    private final DateValidator dateValidator = new DateValidator();

    @Test
    public void validationExceptionWillBeThrownWhenGivenNullDateString() {
        assertThrows(ValidationException.class, () -> dateValidator.validate(null));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenEmptyDateString() {
        assertThrows(ValidationException.class, () -> dateValidator.validate(""));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenDateStringOfInvalidFormat() {
        assertThrows(ValidationException.class, () -> dateValidator.validate("01-01-2020"));
        assertThrows(ValidationException.class, () -> dateValidator.validate("2020.01.01"));
        assertThrows(ValidationException.class, () -> dateValidator.validate("2020-1-1"));
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenValidDateString() {
        assertDoesNotThrow(() -> dateValidator.validate("2020-01-01"));
    }
}