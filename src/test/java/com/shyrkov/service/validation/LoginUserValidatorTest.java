package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.dto.LoginUserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

class LoginUserValidatorTest {

    private final LoginUserValidator validator = new LoginUserValidator();
    private final LoginUserDto userDto = new LoginUserDto();

    @BeforeEach
    public void init() {
        userDto.setEmail("some_email@gmail.com");
        userDto.setPassword("qwerty");
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenLoginUserDtoWithValidData() {
        assertDoesNotThrow(() -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenLoginUserDtoWithNullEmail() {
        userDto.setEmail(null);

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenLoginUserDtoWithBlankEmail() {
        userDto.setEmail("");

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenLoginUserDtoWithInvalidFormatEmail() {
        userDto.setEmail("some_email.com");

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenLoginUserDtoWithNullPassword() {
        userDto.setPassword(null);

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenLoginUserDtoWithBlankPassword() {
        userDto.setPassword("");

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenLoginUserDtoWithTooShortPassword() {
        userDto.setPassword("qwert");

        assertThrows(ValidationException.class, () -> validator.validate(userDto));
    }
}