package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.dto.CategoryDto;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;

@ExtendWith(MockitoExtension.class)
class CategoryValidatorTest {

    @InjectMocks
    private CategoryValidator categoryValidator;
    private final CategoryDto category = new CategoryDto();

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseCategoryDtoWithNullName() {
        category.setName(null);

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseCategoryDtoWithEmptyName() {
        category.setName("");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseCategoryDtoWithTooShortName() {
        category.setName("Ex");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenExpenseCategoryDtoWithNameOfThreeCharacter() {
        category.setName("Exp");

        assertDoesNotThrow(() -> categoryValidator.validate(category));
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenExpenseCategoryDtoWithNameOfInvalidFormat() {
        category.setName("Repair2");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));

        category.setName("Car repair");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));

        category.setName("Car-repair");

        assertThrows(ValidationException.class, () -> categoryValidator.validate(category));
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenExpenseCategoryDtoWithValidName() {
        category.setName("Refuel");

        assertDoesNotThrow(() -> categoryValidator.validate(category));
    }
}