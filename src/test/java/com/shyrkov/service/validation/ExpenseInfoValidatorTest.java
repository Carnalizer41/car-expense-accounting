package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.service.DateFormer;
import com.shyrkov.web.dto.CarInfoDto;
import com.shyrkov.web.dto.CategoryDto;
import com.shyrkov.web.dto.CurrencyDto;
import com.shyrkov.web.dto.SpendingDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class ExpenseInfoValidatorTest {

    @Mock
    private CategoryValidator categoryValidator;
    private final SpendingDto infoDto = new SpendingDto();
    private final CategoryDto categoryDto = new CategoryDto();
    private final DateFormer dateFormer = new DateFormer();
    @InjectMocks
    private ExpenseInfoValidator infoValidator;

    @BeforeEach
    public void init() {
        infoDto.setCategoryDto(categoryDto);
        infoDto.setDate(dateFormer.toString(new Date()));
        infoDto.setSum(0.01);
        infoDto.setCurrency(new CurrencyDto(1L, "Ukrainian Hryvna", "UAH"));
        infoDto.setCategoryDto(new CategoryDto(1L, "REFUEL"));
        infoDto.setCarInfoDto(new CarInfoDto(1L, "123456", "Audi", "e-tron", "RED"));
        infoDto.setDescription("Some description");
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenSpendingDtoWithNullDate() {
        infoDto.setDate(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenSpendingDtoWithNullSum() {
        infoDto.setSum(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenSpendingDtoWithZeroSum() {
        infoDto.setSum(0.0);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenSpendingDtoWithNullCurrency() {
        infoDto.setCurrency(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenSpendingDtoWithNullCategory() {
        infoDto.setCategoryDto(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenSpendingDtoWithNullCar() {
        infoDto.setCarInfoDto(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenSpendingDtoWithNullDescription() {
        infoDto.setDescription(null);

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenSpendingDtoWithBlankDescription() {
        infoDto.setDescription("");

        assertThrows(ValidationException.class, () -> infoValidator.validate(infoDto));

        verifyNoInteractions(categoryValidator);
    }

}