package com.shyrkov.service.validation;

import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.web.dto.RefuelDto;
import com.shyrkov.web.dto.SpendingDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class RefuelValidatorTest {

    @Mock
    private ExpenseInfoValidator infoValidator;
    @Mock
    private CarIdValidator carIdValidator;
    @InjectMocks
    private RefuelValidator refuelValidator;
    private final RefuelDto refuelDto = new RefuelDto();
    private final SpendingDto spendingDto = new SpendingDto();

    @BeforeEach
    public void init() {
        refuelDto.setAmount(0.01);
        refuelDto.setSpendingDto(spendingDto);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenRefuelDtoWithValidData() {
        doNothing().when(infoValidator).validate(refuelDto.getSpendingDto());

        assertDoesNotThrow(() -> refuelValidator.validate(refuelDto));

        verify(infoValidator).validate(refuelDto.getSpendingDto());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenRefuelDtoWithNullVolume() {
        refuelDto.setAmount(null);

        assertThrows(ValidationException.class, () -> refuelValidator.validate(refuelDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(carIdValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenRefuelDtoWithZeroVolume() {
        refuelDto.setAmount(0.0);

        assertThrows(ValidationException.class, () -> refuelValidator.validate(refuelDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(carIdValidator);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenRefuelDtoWithNullSpendingInfo() {
        refuelDto.setSpendingDto(null);

        assertThrows(ValidationException.class, () -> refuelValidator.validate(refuelDto));

        verifyNoInteractions(infoValidator);
        verifyNoInteractions(carIdValidator);
    }
}