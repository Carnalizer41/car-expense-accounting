package com.shyrkov.service.validation;

import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.exceptions.ValidationException;
import com.shyrkov.persistence.CarDao;
import com.shyrkov.web.dto.CarDto;
import com.shyrkov.web.dto.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class CarValidatorTest {

    @Mock
    private CarDao carDao;
    @InjectMocks
    private CarValidator carValidator;
    private final CarDto carInfo = new CarDto();
    private final UserDto userDto = new UserDto();

    @BeforeEach
    public void init() {
        carInfo.setMark("BMW");
        carInfo.setColor("Black");
        carInfo.setModel("M5");
        carInfo.setVinCode("A1234");
        carInfo.setUserEntity(new UserDto());
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenValidCarInfo() {
        doReturn(false).when(carDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carValidator.validate(carInfo));
        assertDoesNotThrow(() -> carValidator.checkExistence(carInfo));

        verify(carDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBlankVinCode() {
        carInfo.setVinCode("");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithNullVinCode() {
        carInfo.setVinCode(null);

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBlankModel() {
        carInfo.setModel("");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithNullModel() {
        carInfo.setModel(null);

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBlankColor() {
        carInfo.setColor("");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithNullColor() {
        carInfo.setColor(null);

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithBlankMark() {
        carInfo.setMark("");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithNullMark() {
        carInfo.setMark(null);

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithVinCodeShorterThanFour() {
        carInfo.setVinCode("123");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCarInfoWithVinCodeOfFourCharacters() {
        carInfo.setVinCode("1234");

        doReturn(false).when(carDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carValidator.validate(carInfo));
        assertDoesNotThrow(() -> carValidator.checkExistence(carInfo));

        verify(carDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithVinCodeOfTwoWords() {
        carInfo.setVinCode("A 1234");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithVinCodeWithUnsupportedCharacters() {
        carInfo.setVinCode("A1234$");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithModelShorterThanThree() {
        carInfo.setModel("G");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCarInfoWithModelOfThreeCharacters() {
        carInfo.setModel("Rio");

        doReturn(false).when(carDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carValidator.validate(carInfo));
        assertDoesNotThrow(() -> carValidator.checkExistence(carInfo));

        verify(carDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithModelWithUnsupportedCharacters() {
        carInfo.setModel("Rio%");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithColorShorterThanThree() {
        carInfo.setColor("Re");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCarInfoWithColorOfThreeCharacters() {
        carInfo.setColor("Red");

        doReturn(false).when(carDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carValidator.validate(carInfo));
        assertDoesNotThrow(() -> carValidator.checkExistence(carInfo));

        verify(carDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithColorWithUnsupportedCharacters() {
        carInfo.setColor("Red%");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithMarkShorterThanThree() {
        carInfo.setMark("BM");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

    @Test
    public void validationWillBeSuccessfulWhenGivenCarInfoWithMarkOfThreeCharacters() {
        carInfo.setMark("BMW");

        doReturn(false).when(carDao).existsByVinCode(carInfo.getVinCode());

        assertDoesNotThrow(() -> carValidator.validate(carInfo));
        assertDoesNotThrow(() -> carValidator.checkExistence(carInfo));

        verify(carDao).existsByVinCode(carInfo.getVinCode());
    }

    @Test
    public void validationExceptionWillBeThrownWhenGivenCarInfoWithMarkWithUnsupportedCharacters() {
        carInfo.setMark("BMW%");

        assertThrows(ValidationException.class, () -> carValidator.validate(carInfo));

        verifyNoInteractions(carDao);
    }

}