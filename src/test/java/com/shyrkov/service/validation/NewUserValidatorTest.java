package com.shyrkov.service.validation;

import com.shyrkov.exceptions.NotUniqueEntityException;
import com.shyrkov.persistence.UserDao;
import com.shyrkov.web.dto.CreateUserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.assertDoesNotThrow;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class NewUserValidatorTest {

    @Mock
    private UserValidator userValidator;
    @Mock
    private UserDao userDao;
    @InjectMocks
    private NewUserValidator newUserValidator;
    private final CreateUserDto userDto = new CreateUserDto();

    @BeforeEach
    public void init() {
        userDto.setEmail("test_email@gmail.com");
        userDto.setPassword("qwe123");
    }

    @Test
    public void notUniqueEntityExceptionWillBeThrownWhenGivenNewUserDtoWithNotUniqueEmail() {
        doNothing().when(userValidator).validate(userDto);
        doReturn(true).when(userDao).existsByEmail(userDto.getEmail());

        assertThrows(NotUniqueEntityException.class, () -> newUserValidator.validate(userDto));

        verify(userDao).existsByEmail(userDto.getEmail());
    }


}