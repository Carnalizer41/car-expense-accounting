package com.shyrkov.service;

import com.shyrkov.persistence.CarDao;
import com.shyrkov.persistence.CategoryDao;
import com.shyrkov.persistence.CurrencyDao;
import com.shyrkov.persistence.SpendingDao;
import com.shyrkov.persistence.entities.CarEntity;
import com.shyrkov.persistence.entities.CategoryEntity;
import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.persistence.entities.SpendingEntity;
import com.shyrkov.service.mappers.*;
import com.shyrkov.service.validation.ExpenseInfoValidator;
import com.shyrkov.web.dto.*;
import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class SpendingServiceTest {

    @Captor
    private ArgumentCaptor<SpendingDto> spendingDtoCaptor;
    @Captor
    private ArgumentCaptor<SpendingEntity> spendingEntityCaptor;
    @Mock
    private ExpenseInfoValidator expenseInfoValidator;
    @Mock
    private SpendingMapper spendingMapper;
    @Mock
    private CategoryDao categoryDao;
    @Mock
    private CarDao carDao;
    @Mock
    private CurrencyDao currencyDao;
    @Mock
    private SpendingDao spendingDao;
    @Mock
    private CategoryMapper categoryMapper;
    @Mock
    private CarInfoMapper carMapper;
    @Mock
    private CurrencyMapper currencyMapper;
    @InjectMocks
    private SpendingService spendingService;

    @Test
    public void newSpendingWillBeSavedAndSpendingDtoWillBeReturned() {
        Long testId = 1L;
        String testDate = "2021-07-21";
        Double testSum = 100.0;
        String testDescription = "Test description";
        CurrencyDto currencyDto = new CurrencyDto(1L, "Ukrainian Hryvna", "UAH");
        CategoryDto categoryDto = new CategoryDto(1L, "Refuel");
        CarInfoDto carInfoDto = new CarInfoDto(1L, "12345", "BMW", "M5", "RED");
        SpendingDto newSpending = new SpendingDto();
        newSpending.setDate(testDate);
        newSpending.setSum(testSum);
        newSpending.setDescription(testDescription);
        newSpending.setCategoryDto(categoryDto);
        newSpending.setCurrency(currencyDto);
        newSpending.setCarInfoDto(carInfoDto);
        CurrencyEntity currencyEntity = new CurrencyEntity();
        currencyEntity.setId(testId);
        currencyEntity.setName("Ukrainian Hryvna");
        currencyEntity.setCode("UAH");
        CategoryEntity categoryEntity = new CategoryEntity();
        categoryEntity.setId(testId);
        categoryEntity.setName("Refuel");
        CarEntity carEntity = new CarEntity();
        carEntity.setId(testId);
        carEntity.setVinCode("12345");
        carEntity.setMark("BMW");
        carEntity.setModel("M5");
        carEntity.setColor("RED");
        SpendingEntity newSpendingEntity = new SpendingEntity();
        newSpendingEntity.setDate(testDate);
        newSpendingEntity.setSum(testSum);
        newSpendingEntity.setDescription(testDescription);
        newSpendingEntity.setCurrencyEntity(currencyEntity);
        newSpendingEntity.setCategoryEntity(categoryEntity);
        newSpendingEntity.setCarEntity(carEntity);
        SpendingEntity savedSpendingEntity = new SpendingEntity();
        savedSpendingEntity.setId(testId);
        savedSpendingEntity.setDate(testDate);
        savedSpendingEntity.setSum(testSum);
        savedSpendingEntity.setDescription(testDescription);
        savedSpendingEntity.setCurrencyEntity(currencyEntity);
        savedSpendingEntity.setCategoryEntity(categoryEntity);
        savedSpendingEntity.setCarEntity(carEntity);
        SpendingDto savedSpendingDto = new SpendingDto();
        savedSpendingDto.setId(testId);
        savedSpendingDto.setDate(testDate);
        savedSpendingDto.setSum(testSum);
        savedSpendingDto.setDescription(testDescription);
        savedSpendingDto.setCategoryDto(categoryDto);
        savedSpendingDto.setCurrency(currencyDto);
        savedSpendingDto.setCarInfoDto(carInfoDto);

        doNothing().when(expenseInfoValidator).validate(newSpending);
        doReturn(newSpendingEntity).when(spendingMapper).dtoToEntity(newSpending);
        doReturn(categoryEntity).when(categoryDao).findOneByName(newSpending.getCategoryDto().getName());
        doReturn(carEntity).when(carDao).findOneById(newSpending.getCarInfoDto().getId());
        doReturn(currencyEntity).when(currencyDao).findOneByCode(newSpending.getCurrency().getCode());
        doReturn(savedSpendingEntity).when(spendingDao).saveAndFlush(newSpendingEntity);
        doReturn(savedSpendingDto).when(spendingMapper).entityToDto(savedSpendingEntity);
        doReturn(categoryDto).when(categoryMapper).entityToDto(categoryEntity);
        doReturn(carInfoDto).when(carMapper).entityToDto(carEntity);
        doReturn(currencyDto).when(currencyMapper).entityToDto(currencyEntity);

        SpendingDto returnedSpending = spendingService.addNewSpending(newSpending);

        verify(expenseInfoValidator).validate(newSpending);
        verify(spendingMapper).dtoToEntity(spendingDtoCaptor.capture());
        verify(spendingDao).saveAndFlush(spendingEntityCaptor.capture());
        verify(spendingMapper).entityToDto(spendingEntityCaptor.capture());

        assertEquals(testDate, spendingDtoCaptor.getValue().getDate());
        assertEquals(testSum, spendingDtoCaptor.getValue().getSum());
        assertEquals(testDescription, spendingDtoCaptor.getValue().getDescription());
        assertEquals(categoryDto, spendingDtoCaptor.getValue().getCategoryDto());
        assertEquals(currencyDto, spendingDtoCaptor.getValue().getCurrency());
        assertEquals(carInfoDto, spendingDtoCaptor.getValue().getCarInfoDto());
        assertEquals(testDate, spendingEntityCaptor.getAllValues().get(0).getDate());
        assertEquals(testSum, spendingEntityCaptor.getAllValues().get(0).getSum());
        assertEquals(testDescription, spendingEntityCaptor.getAllValues().get(0).getDescription());
        assertEquals(categoryEntity, spendingEntityCaptor.getAllValues().get(0).getCategoryEntity());
        assertEquals(currencyEntity, spendingEntityCaptor.getAllValues().get(0).getCurrencyEntity());
        assertEquals(carEntity, spendingEntityCaptor.getAllValues().get(0).getCarEntity());
        assertNull(spendingEntityCaptor.getAllValues().get(0).getId());
        assertEquals(testDate, spendingEntityCaptor.getAllValues().get(1).getDate());
        assertEquals(testSum, spendingEntityCaptor.getAllValues().get(1).getSum());
        assertEquals(testDescription, spendingEntityCaptor.getAllValues().get(1).getDescription());
        assertEquals(categoryEntity, spendingEntityCaptor.getAllValues().get(1).getCategoryEntity());
        assertEquals(currencyEntity, spendingEntityCaptor.getAllValues().get(1).getCurrencyEntity());
        assertEquals(carEntity, spendingEntityCaptor.getAllValues().get(1).getCarEntity());
        assertEquals(testId, spendingEntityCaptor.getAllValues().get(1).getId());
        assertEquals(savedSpendingDto, returnedSpending);
        assertEquals(testDate, returnedSpending.getDate());
        assertEquals(testSum, returnedSpending.getSum());
        assertEquals(testDescription, returnedSpending.getDescription());
        assertEquals(testId, returnedSpending.getId());

    }







}