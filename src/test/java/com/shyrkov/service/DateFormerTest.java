package com.shyrkov.service;

import com.shyrkov.exceptions.DateFormerException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.junit.jupiter.MockitoExtension;

import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.*;

@ExtendWith(MockitoExtension.class)
class DateFormerTest {

    @InjectMocks
    private DateFormer dateFormer;

    @Test
    public void firstOfJanuaryOfTheYearDateWillBeReturnedWhenGivenYear() {
        int year = 2021;

        Date yearHead = dateFormer.getDateByYear(year);
        LocalDate localDate = yearHead.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        assertEquals(2021, localDate.getYear());
        assertEquals(1, localDate.getMonthValue());
        assertEquals(1, localDate.getDayOfMonth());
    }

    @Test
    public void dateWillBeReturnedWhenGivenItsStringRepresentation() {
        String date = "2021-07-21";

        Date realDate = dateFormer.toDate(date);
        LocalDate localDate = realDate.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();

        assertEquals(2021, localDate.getYear());
        assertEquals(7, localDate.getMonthValue());
        assertEquals(21, localDate.getDayOfMonth());
    }

    @Test
    public void dateFormerExceptionWillBeThrownWhenGivenInvalidDateString() {
        String date = "2020-06-35";

        assertThrows(DateFormerException.class, () -> dateFormer.toDate(date));
    }
}


