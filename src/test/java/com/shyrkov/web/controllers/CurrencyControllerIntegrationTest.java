package com.shyrkov.web.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shyrkov.persistence.CurrencyDao;
import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.web.dto.CurrencyDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CurrencyControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CurrencyDao currencyDao;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final CurrencyDto newCurrencyDto = new CurrencyDto();
    private final List<CurrencyEntity> currencyList = new ArrayList<>();
    private final CurrencyEntity firstCurrencyEntity = new CurrencyEntity();
    private final CurrencyEntity secondCurrencyEntity = new CurrencyEntity();

    @BeforeEach
    public void init() {
        newCurrencyDto.setName("Test currency");
        newCurrencyDto.setCode("TCU");

        if (currencyList.isEmpty()) {
            firstCurrencyEntity.setName("First currency");
            firstCurrencyEntity.setCode("FCU");
            secondCurrencyEntity.setName("Second currency");
            secondCurrencyEntity.setCode("SCU");
            currencyList.add(firstCurrencyEntity);
            currencyList.add(secondCurrencyEntity);
        }
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void newCurrencyWillBeSavedAndReturnedDtoWhenGivenValidDataByAuthorizedAsAdmin() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        doAnswer(invocationOnMock -> {
            CurrencyEntity savedCurrencyEntity = invocationOnMock.getArgument(0);
            savedCurrencyEntity.setId(1L);
            return savedCurrencyEntity;
        }).when(currencyDao).save(any(CurrencyEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/currency")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();

        CurrencyDto returnedCurrency = objectMapper.readValue(response.getContentAsString(), CurrencyDto.class);

        verify(currencyDao).save(any(CurrencyEntity.class));

        assertEquals(1L, returnedCurrency.getId());
        assertEquals("Test currency", returnedCurrency.getName());
        assertEquals("TCU", returnedCurrency.getCode());
    }

    @Test
    public void accessWillBeDeniedWhenGivenValidDataByUnauthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        mockMvc.perform(post("/currency")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(currencyDao);
    }

    @Test
    @WithMockUser
    public void accessWillBeDeniedWhenGivenValidDataByAuthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        mockMvc.perform(post("/currency")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(currencyDao);
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenNullName() throws Exception {
        newCurrencyDto.setName(null);
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        MockHttpServletResponse response = mockMvc.perform(post("/currency")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(currencyDao);

        assertEquals("Currency's name is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenBlankName() throws Exception {
        newCurrencyDto.setName("");
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        MockHttpServletResponse response = mockMvc.perform(post("/currency")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(currencyDao);

        assertEquals("Currency's name is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenTooShortName() throws Exception {
        newCurrencyDto.setName("Cur");
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        MockHttpServletResponse response = mockMvc.perform(post("/currency")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(currencyDao);

        assertEquals("Invalid currency's name format", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenInvalidName() throws Exception {
        newCurrencyDto.setName("Currency 1$");
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        MockHttpServletResponse response = mockMvc.perform(post("/currency")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(currencyDao);

        assertEquals("Invalid currency's name format", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenNullCode() throws Exception {
        newCurrencyDto.setCode(null);
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        MockHttpServletResponse response = mockMvc.perform(post("/currency")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(currencyDao);

        assertEquals("Currency's code type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenBlankCode() throws Exception {
        newCurrencyDto.setCode("");
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        MockHttpServletResponse response = mockMvc.perform(post("/currency")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(currencyDao);

        assertEquals("Currency's code type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenTooShortCode() throws Exception {
        newCurrencyDto.setCode("C");
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        MockHttpServletResponse response = mockMvc.perform(post("/currency")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(currencyDao);

        assertEquals("Invalid currency's code format", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenInvalidCode() throws Exception {
        newCurrencyDto.setCode("C 1 %");
        String requestBody = objectMapper.writeValueAsString(newCurrencyDto);

        MockHttpServletResponse response = mockMvc.perform(post("/currency")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(currencyDao);

        assertEquals("Invalid currency's code format", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void listOfCurrencyDtosWillBeReturnedWhenGivenRequestByAuthorizedAdmin() throws Exception {
        doReturn(currencyList).when(currencyDao).findAll();

        MockHttpServletResponse response = mockMvc.perform(get("/currency"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        List<CurrencyDto> currencyDtos = objectMapper.readValue(response.getContentAsString(),
                                                                new TypeReference<ArrayList<CurrencyDto>>() {});

        verify(currencyDao).findAll();

        assertEquals(2, currencyDtos.size());
    }

    @Test
    @WithMockUser
    public void accessWillBeDeniedWhenGivenRequestByAuthorizedUser() throws Exception {

        mockMvc.perform(get("/currency"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(currencyDao);
    }

    @Test
    public void accessWillBeDeniedWhenGivenRequestByUnauthorizedUser() throws Exception {

        mockMvc.perform(get("/currency"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(currencyDao);
    }
}