package com.shyrkov.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shyrkov.persistence.*;
import com.shyrkov.persistence.entities.*;
import com.shyrkov.service.CurrentUserExtractor;
import com.shyrkov.web.dto.*;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class RefuelControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private RefuelDao refuelDao;
    @MockBean
    private SpendingDao spendingDao;
    @MockBean
    private CategoryDao categoryDao;
    @MockBean
    private CarDao carDao;
    @MockBean
    private CurrencyDao currencyDao;
    @MockBean
    private CurrentUserExtractor userExtractor;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final RefuelDto newRefuel = new RefuelDto();
    private final SpendingDto newSpending = new SpendingDto();
    private final CategoryEntity categoryEntity = new CategoryEntity();
    private final CarEntity carEntity = new CarEntity();
    private final CurrencyEntity currencyEntity = new CurrencyEntity();
    private final CarInfoDto carInfoDto = new CarInfoDto();
    private final CurrencyDto currencyDto = new CurrencyDto();
    private final CategoryDto categoryDto = new CategoryDto();
    private final UserEntity userEntity = new UserEntity();

    @BeforeEach
    public void init() {
        carInfoDto.setId(1L);
        currencyDto.setCode("CUR");
        categoryDto.setName("Category");

        newSpending.setSum(100.0);
        newSpending.setDate("2021-01-01");
        newSpending.setDescription("Test description");
        newSpending.setCarInfoDto(carInfoDto);
        newSpending.setCurrency(currencyDto);
        newSpending.setCategoryDto(categoryDto);

        categoryEntity.setId(1L);
        categoryEntity.setName("Category");

        carEntity.setId(1L);
        carEntity.setVinCode("S1221S");
        carEntity.setMark("Mark");
        carEntity.setModel("Model");
        carEntity.setColor("Color");
        carEntity.setUserEntity(new UserEntity());

        currencyEntity.setId(1L);
        currencyEntity.setName("Currency");
        currencyEntity.setCode("CUR");

        newRefuel.setAmount(10.0);
        newRefuel.setSpendingDto(newSpending);

        userEntity.setId(1L);
    }

    @Test
    @WithMockUser
    public void newSpendingAndRefuelWillBeSavedAndDtoWillBeReturnedWhenGivenRefuelDtoWithValidDataByAuthorizedUser()
            throws Exception {
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        doReturn(categoryEntity).when(categoryDao).findOneByName(newSpending.getCategoryDto().getName());
        doReturn(carEntity).when(carDao).findOneById(newSpending.getCarInfoDto().getId());
        doReturn(currencyEntity).when(currencyDao).findOneByCode(newSpending.getCurrency().getCode());

        doAnswer(invocationOnMock -> {
            SpendingEntity savedSpendingEntity = invocationOnMock.getArgument(0);
            savedSpendingEntity.setId(1L);
            return savedSpendingEntity;
        }).when(spendingDao).saveAndFlush(any(SpendingEntity.class));

        doAnswer(invocationOnMock -> {
            RefuelEntity savedRefuelEntity = invocationOnMock.getArgument(0);
            savedRefuelEntity.setId(1L);
            return savedRefuelEntity;
        }).when(refuelDao).save(any(RefuelEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        RefuelDto refuelDto = objectMapper.readValue(response.getContentAsString(), RefuelDto.class);

        verify(categoryDao).findOneByName(newSpending.getCategoryDto().getName());
        verify(carDao).findOneById(newSpending.getCarInfoDto().getId());
        verify(currencyDao).findOneByCode(newSpending.getCurrency().getCode());
        verify(spendingDao).saveAndFlush(any(SpendingEntity.class));
        verify(refuelDao).save(any(RefuelEntity.class));

        assertEquals(1L, refuelDto.getId());
        assertEquals(1L, refuelDto.getSpendingDto().getId());
        assertEquals("2021-01-01", refuelDto.getSpendingDto().getDate());
        assertEquals(100.0, refuelDto.getSpendingDto().getSum());
        assertEquals("Test description", refuelDto.getSpendingDto().getDescription());
        assertEquals(10.0, refuelDto.getAmount());
    }

    @Test
    public void accessWillBeDeniedWhenGivenRefuelDtoWithValidDataByUnauthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        mockMvc.perform(post("/refuels")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(categoryDao);
        verifyNoInteractions(carDao);
        verifyNoInteractions(refuelDao);
        verifyNoInteractions(spendingDao);
        verifyNoInteractions(currencyDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullAmount() throws Exception {
        newRefuel.setAmount(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Refuel volume is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenTooSmallAmount() throws Exception {
        newRefuel.setAmount(0.0);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Refuel volume is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullSpendingInfo() throws Exception {
        newRefuel.setSpendingDto(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Refuel info is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullSpendingSum() throws Exception {
        newSpending.setSum(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Expense sum is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenTooSmallSpendingSum() throws Exception {
        newSpending.setSum(0.0);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Expense sum is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullSpendingDate() throws Exception {
        newSpending.setDate(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Expense date is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullSpendingCurrency() throws Exception {
        newSpending.setCurrency(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Currency is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullSpendingCar() throws Exception {
        newSpending.setCarInfoDto(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Car is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullSpendingDescription() throws Exception {
        newSpending.setDescription(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Expense description is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenBlankSpendingDescription() throws Exception {
        newSpending.setDescription("");
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Expense description is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullSpendingCategory() throws Exception {
        newSpending.setCategoryDto(null);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Expense category is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullSpendingCategoryName() throws Exception {
        categoryDto.setName(null);
        newSpending.setCategoryDto(categoryDto);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenBlankSpendingCategoryName() throws Exception {
        categoryDto.setName("");
        newSpending.setCategoryDto(categoryDto);
        String requestBody = objectMapper.writeValueAsString(newRefuel);

        MockHttpServletResponse response = mockMvc.perform(post("/refuels")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void refuelAmountWillBeReturnedWhenGivenValidDataByAuthorizedUser() throws Exception {
        doReturn(100.0).when(refuelDao).findAmountByCar(eq(1L),
                                                        any(String.class),
                                                        any(String.class));

        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_car")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        RefuelAmountDto refuelAmount = objectMapper.readValue(response.getContentAsString(),
                                                              RefuelAmountDto.class);

        verify(refuelDao).findAmountByCar(eq(1L), any(String.class), any(String.class));

        assertEquals(100.0, refuelAmount.getAmount());
    }

    @Test
    public void accessWillBeDeniedWhenGivenValidDataByUnauthorizedUser() throws Exception {
        mockMvc.perform(get("/refuels/by_car")
                                .param("car_id", "1")
                                .param("start_date", "2020-07-01")
                                .param("end_date", "2020-07-31"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(refuelDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenEmptyCarId() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_car")
                                                                   .param("car_id", "")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenZeroCarId() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_car")
                                                                   .param("car_id", "0")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Car id is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutCarIdParam() throws Exception {
        mockMvc.perform(get("/refuels/by_car")
                                .param("start_date", "2020-07-01")
                                .param("end_date", "2020-07-31"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(refuelDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenEmptyStartDate() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_car")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Date is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenInvalidFormatStartDate() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_car")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "01-07-2020")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Invalid date format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutStartDateParam() throws Exception {
        mockMvc.perform(get("/refuels/by_car")
                                .param("car_id", "1")
                                .param("end_date", "2020-07-31"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(refuelDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenEmptyEndDate() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_car")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", ""))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Date is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenInvalidFormatEndDate() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_car")
                                                                   .param("car_id", "1")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", "31-07-2020"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Invalid date format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutEndDateParam() throws Exception {
        mockMvc.perform(get("/refuels/by_car")
                                .param("car_id", "1")
                                .param("start_date", "2020-07-31"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(refuelDao);
    }

    @Test
    public void accessToAmountByUserWillBeDeniedWhenGivenValidDataByUnauthorizedUser() throws Exception {
        mockMvc.perform(get("/refuels/by_user")
                                .param("start_date", "2020-07-01")
                                .param("end_date", "2020-07-31"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(refuelDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenEmptyStartDateToFindAmountByUser() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_user")
                                                                   .param("start_date", "")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Date is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenInvalidFormatStartDateToFindAmountByUser() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_user")
                                                                   .param("start_date", "01-07-2020")
                                                                   .param("end_date", "2020-07-31"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Invalid date format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutStartDateParamToFindAmountByUser() throws Exception {
        mockMvc.perform(get("/refuels/by_user")
                                .param("end_date", "2020-07-31"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(refuelDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenEmptyEndDateToFindAmountByUser() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_user")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", ""))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Date is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenInvalidFormatEndDateToFindAmountByUser() throws Exception {
        MockHttpServletResponse response = mockMvc.perform(get("/refuels/by_user")
                                                                   .param("start_date", "2020-07-01")
                                                                   .param("end_date", "31-07-2020"))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(refuelDao);

        assertEquals("Invalid date format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenRequestWithoutEndDateParamToFindAmountByUser() throws Exception {
        mockMvc.perform(get("/refuels/by_user")
                                .param("start_date", "2020-07-31"))
               .andExpect(status().isBadRequest());

        verifyNoInteractions(refuelDao);
    }
}