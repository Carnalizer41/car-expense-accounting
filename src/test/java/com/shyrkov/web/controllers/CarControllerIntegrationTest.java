package com.shyrkov.web.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shyrkov.persistence.CarDao;
import com.shyrkov.persistence.entities.CarEntity;
import com.shyrkov.persistence.entities.CurrencyEntity;
import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.service.CurrentUserExtractor;
import com.shyrkov.web.dto.CarDto;
import com.shyrkov.web.dto.CurrencyDto;
import com.shyrkov.web.dto.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoInteractions;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
class CarControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CarDao carDao;
    @MockBean
    private CurrentUserExtractor userExtractor;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final CarDto newCar = new CarDto();
    private final List<CarEntity> cars = new ArrayList<>();
    private final CarEntity savedCar = new CarEntity();
    private final UserEntity userEntity = new UserEntity();
    private final UserDto userDto = new UserDto();

    @BeforeEach
    public void init() {
        userDto.setCurrency(new CurrencyDto());

        newCar.setVinCode("123456");
        newCar.setMark("TestMark");
        newCar.setModel("Test model 1");
        newCar.setColor("Red");
        newCar.setUserEntity(userDto);

        userEntity.setId(1L);
        userEntity.setCurrency(new CurrencyEntity());

        savedCar.setId(1L);
        savedCar.setVinCode("123456");
        savedCar.setMark("TestMark");
        savedCar.setModel("Test model 1");
        savedCar.setColor("Red");
        savedCar.setUserEntity(userEntity);

        if (cars.isEmpty()) {
            for (int i = 0; i < 4; i++) {
                CarEntity carEntity = new CarEntity();
                carEntity.setUserEntity(userEntity);
                cars.add(carEntity);
            }
        }
    }


    @Test
    @WithMockUser
    public void newCarWillBeSavedAndDtoWithIdWillBeReturnedWhenGivenCarDtoWithValidDataByAuthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCar);

        doReturn(false).when(carDao).existsByVinCode(newCar.getVinCode());
        doReturn(new UserEntity()).when(userExtractor).getCurrentUser();
        doReturn(savedCar).when(carDao).save(any(CarEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        CarDto responseDto = objectMapper.readValue(response.getContentAsString(), CarDto.class);

        verify(carDao).existsByVinCode(newCar.getVinCode());
        verify(carDao).save(any(CarEntity.class));

        assertEquals(1L, responseDto.getId());
        assertEquals("123456", responseDto.getVinCode());
        assertEquals("TestMark", responseDto.getMark());
        assertEquals("Test model 1", responseDto.getModel());
        assertEquals("Red", responseDto.getColor());
    }

    @Test
    public void accessWillBeDeniedWhenGivenCarDtoWithValidDataByUnauthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCar);

        mockMvc.perform(post("/cars")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(carDao);
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullVinCode() throws Exception {
        newCar.setVinCode(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Car's vin code is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenBlankVinCode() throws Exception {
        newCar.setVinCode("");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Car's vin code is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenTooShortVinCode() throws Exception {
        newCar.setVinCode("123");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Invalid car's vin code format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenInvalidFormatVinCode() throws Exception {
        newCar.setVinCode("# QS4586");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Invalid car's vin code format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullMark() throws Exception {
        newCar.setMark(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Car's mark is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenBlankMark() throws Exception {
        newCar.setMark("");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Car's mark is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenTooShortMark() throws Exception {
        newCar.setMark("M");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Invalid car's mark format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithInvalidFormatMark() throws Exception {
        newCar.setMark("Invalid_car_mark 1");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Invalid car's mark format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullModel() throws Exception {
        newCar.setModel(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Car's model is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenBlankModel() throws Exception {
        newCar.setModel("");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Car's model is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenCarDtoWithInvalidFormatModel() throws Exception {
        newCar.setModel("Invalid_car_model");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Invalid car's model format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNullColor() throws Exception {
        newCar.setColor(null);
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Car's color is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenBlankColor() throws Exception {
        newCar.setColor("");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Car's color is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenTooShortColor() throws Exception {
        newCar.setColor("Re");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Invalid car's color format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenInvalidFormatColor() throws Exception {
        newCar.setColor("Wet asphalt 18");
        String requestBody = objectMapper.writeValueAsString(newCar);

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(carDao);

        assertEquals("Invalid car's color format", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void clientErrorWillBeReturnedWhenGivenNotUniqueVinCode() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCar);

        doReturn(true).when(carDao).existsByVinCode(newCar.getVinCode());

        MockHttpServletResponse response = mockMvc.perform(post("/cars")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(carDao).existsByVinCode(newCar.getVinCode());
        verifyNoMoreInteractions(carDao);

        assertEquals("Car with such vin code already exists", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void listOfCarDtosWillBeReturnedWhenGivenRequestByAuthorizedUser() throws Exception {
        doReturn(userEntity).when(userExtractor).getCurrentUser();
        doReturn(cars).when(carDao).findAll();

        MockHttpServletResponse response = mockMvc.perform(get("/cars"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        List<CarDto> carDtos = objectMapper.readValue(response.getContentAsString(),
                                                      new TypeReference<ArrayList<CarDto>>() {});

        verify(carDao).findAll();

        assertEquals(4, carDtos.size());
    }


    @Test
    @WithMockUser()
    public void accessToRatingWillBeDeniedWhenUserIsNotAdmin() throws Exception {
        mockMvc.perform(get("/cars/rating"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(carDao);
    }
}