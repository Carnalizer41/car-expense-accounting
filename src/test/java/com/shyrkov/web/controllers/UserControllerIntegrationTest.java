package com.shyrkov.web.controllers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.shyrkov.persistence.UserDao;
import com.shyrkov.persistence.entities.UserEntity;
import com.shyrkov.web.dto.CreateUserDto;
import com.shyrkov.web.dto.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.mockito.Mockito.verifyNoMoreInteractions;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@SpringBootTest
@AutoConfigureMockMvc
class UserControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private UserDao userDao;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final CreateUserDto newUser = new CreateUserDto();

    @BeforeEach
    public void init() {
        newUser.setName("example name");
        newUser.setPhone("+1234567890");
        newUser.setEmail("some_email@gmail.com");
        newUser.setPassword("qwerty");
    }

    @Test
    public void newUserWillBeCreatedAndUserDtoWillBeReturnedWhenGivenNewUserCredentials() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newUser);

        doReturn(false).when(userDao).existsByEmail(newUser.getEmail());
        doAnswer(invocationOnMock -> {
            UserEntity userEntity = invocationOnMock.getArgument(0);
            userEntity.setId(1L);
            return userEntity;
        }).when(userDao).save(any(UserEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        UserDto returnedUser = objectMapper.readValue(response.getContentAsString(), UserDto.class);

        verify(userDao).existsByEmail(newUser.getEmail());
        verify(userDao).save(any(UserEntity.class));

        assertEquals(1L, returnedUser.getId());
        assertEquals("some_email@gmail.com", returnedUser.getEmail());
    }


    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithNullEmail() throws Exception {
        newUser.setEmail(null);
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Email is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithBlankEmail() throws Exception {
        newUser.setEmail("");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Email is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithInvalidFormatEmail() throws Exception {
        newUser.setEmail("some_email.com");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Invalid email format", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithNullPassword() throws Exception {
        newUser.setPassword(null);
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Password is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithBlankPassword() throws Exception {
        newUser.setPassword("");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Password is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithTooShortPassword() throws Exception {
        newUser.setPassword("qwert");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Too short password", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithNullName() throws Exception {
        newUser.setName(null);
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("User's name is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithBlankName() throws Exception {
        newUser.setName("");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("User's name is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithTooShortName() throws Exception {
        newUser.setName("A");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Invalid user's name format", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithInvalidNameFormat() throws Exception {
        newUser.setName("A2$");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Invalid user's name format", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithNullPhone() throws Exception {
        newUser.setPhone(null);
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("User's phone is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithBlankPhone() throws Exception {
        newUser.setPhone("");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("User's phone is missing", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithTooShortPhone() throws Exception {
        newUser.setPhone("123456789");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Invalid user's phone format", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithTooLongPhone() throws Exception {
        newUser.setPhone("+3809911223344");
        String requestBody = objectMapper.writeValueAsString(newUser);

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(userDao);

        assertEquals("Invalid user's phone format", response.getContentAsString());
    }

    @Test
    public void clientErrorWillBeReturnedWhenGivenNewUserWithNotUniqueEmail() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newUser);

        doReturn(true).when(userDao).existsByEmail(newUser.getEmail());

        MockHttpServletResponse response = mockMvc.perform(post("/users")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(userDao).existsByEmail(newUser.getEmail());
        verifyNoMoreInteractions(userDao);

        assertEquals("User with such email already exists", response.getContentAsString());
    }

}