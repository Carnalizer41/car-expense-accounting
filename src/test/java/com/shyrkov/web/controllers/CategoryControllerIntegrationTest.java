package com.shyrkov.web.controllers;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.shyrkov.persistence.CategoryDao;
import com.shyrkov.persistence.entities.CategoryEntity;
import com.shyrkov.web.dto.CategoryDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.mock.web.MockHttpServletResponse;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
class CategoryControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private CategoryDao categoryDao;
    private final ObjectMapper objectMapper = new ObjectMapper();
    private final CategoryDto newCategoryDto = new CategoryDto();
    private final List<CategoryEntity> categoryList = new ArrayList<>();
    private final CategoryEntity firstCategoryEntity = new CategoryEntity();
    private final CategoryEntity secondCategoryEntity = new CategoryEntity();

    @BeforeEach
    public void init() {
        newCategoryDto.setName("Test category");

        if (categoryList.isEmpty()) {
            firstCategoryEntity.setName("First category");
            secondCategoryEntity.setName("Second category");
            categoryList.add(firstCategoryEntity);
            categoryList.add(secondCategoryEntity);
        }
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void newCategoryWillBeSavedAndReturnedDtoWhenGivenValidDataAndAuthorizedAsAdmin() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCategoryDto);

        doReturn(false).when(categoryDao).existsByExpenseType(newCategoryDto.getName());
        doAnswer(invocationOnMock -> {
            CategoryEntity savedCategoryEntity = invocationOnMock.getArgument(0);
            savedCategoryEntity.setId(1L);
            return savedCategoryEntity;
        }).when(categoryDao).save(any(CategoryEntity.class));

        MockHttpServletResponse response = mockMvc.perform(post("/category")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();

        CategoryDto returnedCategory = objectMapper.readValue(response.getContentAsString(), CategoryDto.class);

        verify(categoryDao).existsByExpenseType(newCategoryDto.getName());
        verify(categoryDao).save(any(CategoryEntity.class));

        assertEquals(1L, returnedCategory.getId());
        assertEquals("Test category", returnedCategory.getName());
    }

    @Test
    public void accessWillBeDeniedWhenGivenValidDataByUnauthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCategoryDto);

        mockMvc.perform(post("/category")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(categoryDao);
    }

    @Test
    @WithMockUser
    public void accessWillBeDeniedWhenGivenValidDataByAuthorizedUser() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCategoryDto);

        mockMvc.perform(post("/category")
                                .contentType(APPLICATION_JSON)
                                .content(requestBody))
               .andExpect(status().isForbidden());

        verifyNoInteractions(categoryDao);
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenNullName() throws Exception {
        newCategoryDto.setName(null);
        String requestBody = objectMapper.writeValueAsString(newCategoryDto);

        MockHttpServletResponse response = mockMvc.perform(post("/category")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenBlankName() throws Exception {
        newCategoryDto.setName("");
        String requestBody = objectMapper.writeValueAsString(newCategoryDto);

        MockHttpServletResponse response = mockMvc.perform(post("/category")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);

        assertEquals("Expense type is missing", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenTooShortName() throws Exception {
        newCategoryDto.setName("Ca");
        String requestBody = objectMapper.writeValueAsString(newCategoryDto);

        MockHttpServletResponse response = mockMvc.perform(post("/category")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);

        assertEquals("Invalid expense type format", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenInvalidName() throws Exception {
        newCategoryDto.setName("Category1$");
        String requestBody = objectMapper.writeValueAsString(newCategoryDto);

        MockHttpServletResponse response = mockMvc.perform(post("/category")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verifyNoInteractions(categoryDao);

        assertEquals("Invalid expense type format", response.getContentAsString());
    }

    @Test
    @WithMockUser(value = "Admin", roles = {"ADMIN"})
    public void clientErrorWillBeReturnedWhenGivenNotUniqueName() throws Exception {
        String requestBody = objectMapper.writeValueAsString(newCategoryDto);

        doReturn(true).when(categoryDao).existsByExpenseType(newCategoryDto.getName());

        MockHttpServletResponse response = mockMvc.perform(post("/category")
                                                                   .contentType(APPLICATION_JSON)
                                                                   .content(requestBody))
                                                  .andExpect(status().isBadRequest())
                                                  .andReturn()
                                                  .getResponse();

        verify(categoryDao).existsByExpenseType(newCategoryDto.getName());
        verifyNoMoreInteractions(categoryDao);

        assertEquals("Such an expense category already exists", response.getContentAsString());
    }

    @Test
    @WithMockUser
    public void listOfCategoryDtosWillBeReturnedWhenGivenRequestByAuthorizedUser() throws Exception {
        doReturn(categoryList).when(categoryDao).findAll();

        MockHttpServletResponse response = mockMvc.perform(get("/category"))
                                                  .andExpect(status().isOk())
                                                  .andReturn()
                                                  .getResponse();
        List<CategoryDto> categoryDtos = objectMapper.readValue(response.getContentAsString(),
                                                      new TypeReference<ArrayList<CategoryDto>>() {});

        verify(categoryDao).findAll();

        assertEquals(2, categoryDtos.size());
    }

    @Test
    public void accessWillBeDeniedWhenGivenRequestByUnauthorizedUser() throws Exception {

        mockMvc.perform(get("/category"))
               .andExpect(status().isForbidden());

        verifyNoInteractions(categoryDao);
    }

}