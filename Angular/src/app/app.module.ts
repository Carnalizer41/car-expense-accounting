import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HttpClientModule} from '@angular/common/http';
import {AppComponent} from './app.component';
import {AddUserComponent} from './component/add-user/add-user.component';
import {UserServiceService} from './service/user-service.service';
import {LoginUserComponent} from './component/login-user/login-user.component';
import {LoginService} from './service/login.service';

@NgModule({
  declarations: [
    AppComponent,
    AddUserComponent,
    LoginUserComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    UserServiceService,
    LoginService
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
