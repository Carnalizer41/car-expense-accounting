import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {
  FormControl,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from '@angular/forms';
import {LoginService} from '../../service/login.service';
import {Login} from '../../model/login';

@Component({
  selector: 'app-login-user',
  templateUrl: './login-user.component.html',
  styleUrls: ['./login-user.component.css']
})
export class LoginUserComponent implements OnInit {

  constructor(private loginService: LoginService,
              private router: Router) {
  }

  login: Login = new Login();

  ngOnInit() {
  }

  loginform = new FormGroup({
    email: new FormControl(),
    password: new FormControl(),
  });

  loginUser(loginUser) {
    this.login = new Login();
    this.login.email = this.loginEmail.value;
    this.login.password = this.loginPassword.value;
    this.log();
  }

  log() {
    this.loginService.login(this.login).subscribe((res: HttpResponse<any>) => {
      localStorage.setItem('Authorization', res.headers.get('Authorization'));
      console.log(localStorage.getItem('Authorization'));
      console.log(res);
    });

  }

  gotoLogin() {
    this.router.navigate(['/login']);
  }

  get loginEmail() {
    return this.loginform.get('email');
  }

  get loginPassword() {
    return this.loginform.get('password');
  }
}
