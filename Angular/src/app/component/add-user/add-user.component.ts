import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {
  FormControl,
  FormGroup,
  Validators,
  ReactiveFormsModule
} from '@angular/forms';
import {UserServiceService} from '../../service/user-service.service';
import {User} from '../../model/user';

@Component({
  selector: 'app-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.css']
})
export class AddUserComponent implements OnInit {

  constructor(private userService: UserServiceService,
              private router: Router) {
  }

  user: User = new User();

  ngOnInit() {
  }

  usersaveform = new FormGroup({
    name: new FormControl('', [Validators.required, Validators.minLength(2)]),
    email: new FormControl('', [Validators.required, Validators.email]),
    phone: new FormControl('', [Validators.required, Validators.minLength(10)]),
    password: new FormControl('', [Validators.required, Validators.minLength(6)]),
    currency: new FormControl()
  });

  saveUser(saveUser) {
    this.user = new User();
    this.user.name = this.userName.value;
    this.user.email = this.userEmail.value;
    this.user.phone = this.userPhone.value;
    this.user.password = this.userPassword.value;
    this.user.currency = this.userCurrency.value;
    this.save();
  }


  save() {
    this.userService.createUser(this.user).subscribe(result => this.gotoLogin());
  }


  gotoLogin() {
    this.router.navigate(['/login']);
  }

  get userName() {
    return this.usersaveform.get('name');
  }

  get userEmail() {
    return this.usersaveform.get('email');
  }

  get userPhone() {
    return this.usersaveform.get('phone');
  }

  get userPassword() {
    return this.usersaveform.get('password');
  }

  get userCurrency() {
    return this.usersaveform.get('currency');
  }
}
