import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders, HttpResponse} from '@angular/common/http';
import {Login} from '../model/login';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class LoginService {

  private loginUrl: string;

  constructor(private http: HttpClient) {
    this.loginUrl = 'http://localhost:8081/login';
  }

  public login(login: Login) {

    const httpOptions = {
      headers: new HttpHeaders({'Content-Type': 'application/json'}),
      observe: 'response' as 'response',
      responseType: 'text'
    };

    return this.http.post<Login>(this.loginUrl, login, {
      headers: new HttpHeaders({'Content-Type': 'application/json'}),
      observe: 'response' as 'response',
      responseType: 'text'
    })
  }


}
