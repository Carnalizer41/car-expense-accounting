import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '../model/user';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class UserServiceService {

  private usersUrl: string;

   constructor(private http: HttpClient) {
     this.usersUrl = 'http://localhost:8081/users';
   }

  public getUserInfo(): Observable<User> {
    return this.http.get<User>(this.usersUrl);
  }

  public editUserInfo(user: User) {
    return this.http.put<User>(this.usersUrl, user);
  }

  public createUser(user: User) {
    return this.http.post<User>(this.usersUrl, user);
  }
}
