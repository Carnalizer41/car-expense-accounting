# CarExpenseAccounting App using Java and Spring

# Description

This application's main purpose is to store information about the car's maintenance history.
It allows users to create the car's profile and then add information about its refuels and repairs whenever they take place.

It also makes it possible to obtain the following information:
- a total volume of gas consumed by a car during a specified period of time;
- an amount of money spent on spare parts for a car during a specified year;
- a total amount of money spent on spare parts for a car;
- a full list of spare parts replaced in a car during its operational history;
- an expense history by category;
- a car mark reliability rating (admin only);
- an information about average amount of brakage and cost on it`s repair by car mark;
- a statistics of repairing separate parts by car mark.

All the information about spent money may be stored and acquired in different currencies (UAH, USD, RUB) according to current exchange rates.

# Used technologies

* Spring Core
* Spring Boot Web 
* Spring Boot Security
* Spring Boot Data JPA
* Spring Boot Test
* JWT
* MapStruct
* Lombok
* Flyway
* PostgreSQL
* Maven

# How it works

- The application follows MVC structure and has three distinguished layers (Web, Service, and Dao).
- Each layer has its own POJO objects to represent data, which are converted into one another by mappers, based on MapStruct.
- Every user input is validated by validation layer using Apache Commons Validator.
- Up-to-date currency exchange rates are obtained from minfin.com.ua API.

The code is organized as follows:

1. `web` is the web layer implemented by Spring Boot Web. It consists of REST controllers, DTOs, etc.
2. `service` is the service layer including business logic classes, validators, and mappers. All of them are Spring Beans.
3. `dao` is the persistence layer for working with a database. It consists of entities, query results and repositories, and utilizes Spring Boot Data JPA.
4. `exceptions` contains all the custom exceptions thrown in application and handled by Exception Handler.
5. `enums` includes ENUMs used in application.
6. `configuration` holds configuration classes.

# Security

Application security is based on Spring Boot Security. 
It supports user authentication, authorization, and uses JWT to generate tokens.
Furthermore, the application maintains two roles (USER and ADMIN) and enables global method security.

The secret token key is stored in `application.properties`.

# Database

It uses a PostgreSQL database. However, it can be changed easily in the `application.properties` for any other database.

# Getting started

In order to be able to use this application one needs to have Java 11 and PostgreSQL installed.
Also, it might be necessary to change DB credentials in `application.properties`. 

# API Specification

### Registration:
`POST /users`

Creates new user. No authentication required.

Request body example:
```JSON
{
    "email":"exampleUser@gmail.com",
    "name":"User Example",
    "password":"emapleUser",
    "phone":"+1244567899",
    "currency":"USD"
}
```
Response body example:
```JSON
{
    "id": 15,
    "name": "User Example",
    "email": "exampleUser@gmail.com",
    "phone": "+1244567899",
    "currency": {
        "id": 2,
        "name": "United States Dollar",
        "code": "USD"
    }
}
```
### Authentication:
`POST /login`

Authenticates user, returns token. No authentication required.

Request body example:
```JSON
{
  "email":"exampleUser@gmail.com",
    "password":"emapleUser"
}
```
Response header example:

`Key: Authorization; Value: soMeENorMouSTokEn`

### Adding new expense category:
`POST /category`

Creates new expense category. Requires authentication and role ADMIN.

Request body example:
```JSON
{
    "name":"Parking"
}
```
Response body example:
```JSON
{
    "id": 3,
    "name": "Parking"
}
```

### Editing existing expense category:
`PUT /category`

Updated expense category. Requires authentication and role ADMIN.

Request body example:
```JSON
{
    "oldName":"Parking",
    "newName":"PARKING"
}
```
Response body example:
```JSON
{
    "id": 3,
    "name": "PARKING"
}
```

### Getting list of available expense categories:
`GET /category`

Returns full list of available expense categories. Requires authentication.

No request body or parameters required.

Example response body:
```JSON
[
    {
        "id": 1,
        "name": "REFUEL"
    },
    {
        "id": 2,
        "name": "REPAIR"
    },
    {
        "id": 3,
        "name": "PARKING"
    }
]
```
### Creating new car profile:
`POST /cars`

Creates new car profile. Requires authentication.

Request body example:
```JSON
{
    "vin_code":"A1221Z",
    "mark":"BMW",
    "model":"M5",
    "color":"white"
}
```
Response body example:
```JSON
{
    "id": 4,
    "mark": "BMW",
    "model": "M5",
    "color": "white",
    "vin_code": "A1221Z"
}
```
### Editing user`s car profile:
`POST /cars/edit`

Updates car profile. Requires authentication.

Request body example:
```JSON
{
    "id":"4",
    "vin_code":"A1221Z",
    "mark":"BMW",
    "model":"M5",
    "color":"red"
}
```
Response body example:
```JSON
{
    "id": 4,
    "mark": "BMW",
    "model": "M5",
    "color": "red",
    "vin_code": "A1221Z"
}
```
### Removing user`s car profile:
`POST /cars/delete_one`

Removes car profile. Requires authentication.

Request body example:
```JSON
{
    "id":"4"
}
```
Response body example:
```JSON
{
    "id": 4,
    "mark": "BMW",
    "model": "M5",
    "color": "white",
    "vin_code": "A1221Z"
}
```
### Getting list of cars:
`GET /cars`

Returns the list of available cars. Requires authentication.

No request body or parameters required.

Response body example:
```JSON
[
    {
        "id": 7,
        "mark": "BMW",
        "model": "M5",
        "color": "white",
        "vin_code": "A1221Z"
    }
]
```
### Adding new refuel to a car:
`POST /refuels`

Adds new refuel to the car's refuel history. Requires authentication.

Request body example:
```JSON
{
    "amount":200.0,
    "spendingDto":{
      "categoryDto":{
          "name":"REFUEL"
      },
      "carInfoDto":{
          "id":7
      },
      "date":"2021-07-21",
      "sum":500,
      "currency":{
          "code":"USD"
       },
      "description":"Gasoline type A-95"
  }
}
```
Response body example:
```JSON
{
    "id": 3,
    "amount": 200.0,
    "spendingDto": {
        "id": 5,
        "date": "2021-07-21",
        "sum": 500.0,
        "currency": {
            "id": 2,
            "name": "United States Dollar",
            "code": "USD"
        },
        "description": "Gasoline type A-95",
        "categoryDto": {
            "id": 1,
            "name": "REFUEL"
        },
        "carInfoDto": {
            "id": 7,
            "mark": "BMW",
            "model": "M5",
            "color": "white",
            "vin_code": "A1221Z"
        }
    }
}
```

### Getting volume of gas consumed by a car and user during a specified period:
`GET /refuels/by_car`

Returns volume of gas consumed by a car during a specified period. Requires authentication.

Query parameters example:

`?car_id=7&start_date=2021-06-23&end_date=2021-07-23`

where `car_id` specifies a car, `start_date` and `end_date` set a desired period.

Response body example:
```JSON
{
    "amount": 200.0
}
```

`GET /refuels/by_user`

Returns volume of gas consumed by all user`s cars during a specified period. Requires authentication.

Query parameters example:

`?start_date=2021-06-23&end_date=2021-07-23`

where `start_date` and `end_date` set a desired period.

Response body example:
```JSON
{
    "amount": 200.0
}
```

### Adding new repair to a car:
`POST /repaired_details`

Adds new repair to the car's repair history. Requires authentication.

Request body example:
```JSON
{
    "name":"Wheels",
    "serialNumber":"1234",
    "spendingDto":{
      "categoryDto":{
          "name":"REPAIR"
      },
      "carInfoDto":{
          "id":7
      },
      "date":"2020-09-23",
      "sum":300,
      "currency":{
          "code":"USD"
       },
      "description":"Dismanting wheels"
  }
}
```
Response body example:
```JSON
{
    "id": 3,
    "name": "Wheels",
    "serialNumber": "1234",
    "spendingDto": {
        "id": 7,
        "date": "2020-09-23",
        "sum": 300.0,
        "currency": {
            "id": 2,
            "name": "United States Dollar",
            "code": "USD"
        },
        "description": "Dismanting wheels",
        "categoryDto": {
            "id": 2,
            "name": "REPAIR"
        },
        "carInfoDto": {
            "id": 7,
            "mark": "BMW",
            "model": "M5",
            "color": "white",
            "vin_code": "A1221Z"
        }
    }
}
```

### Getting an amount of money spent on spare parts during a specified year:
`GET /repaired_details/by_car`

Returns amount of money spent on spare parts for a car during a specified year. Requires authentication.

Query parameters example:

`?car_id=7&year=2021&currency=USD`

where `car_id` specifies a car, `year` sets a desired year, `currency` selects a currency of response.

Response body example:
```JSON
{
    "moneySpent": 202.2691
}
```
### Getting total amount of money spent on spare parts:
`GET /repaired_details/by_user`

Returns total amount of money spent on spare parts by user during a specified year. Requires authentication.

Query parameters example:

`?year=2020&currency=UAH`

where `currency` selects a currency of response, empty `year` sets a desired year.

Response body example:
```JSON
{
    "moneySpent": 8157.45
}
```

### Getting a list of replaced parts:
`GET /repaired_details/list/by_car`

Query parameters example:

`?car_id=7`

where `car_id` specifies a car.

Returns a list of car's replaced parts. Requires authentication.

Response body example:
```JSON
[
    {
        "name": "Windows",
        "serialNumber": "121212"
    },
    {
        "name": "Wheels",
        "serialNumber": "1234"
    }
]
```

### Getting total sum of money spent by category:
`GET /spending/sum_by_category`

Query parameters example:

`?carId=7&category=REFUEL&currency=USD`

where `car_id` specifies a car, `category` specifies an expense caregory to look for, `currency` selects a currency of response.

Returns a list of car's replaced parts. Requires authentication.

Response body example:
```JSON
{
    "moneySpent": 500.0
}
```

### Getting expense history by category:
`GET /spending/history_by_category`

Query parameters example:

`?category=REPAIR&car_id=7`

where `car_id` specifies a car, `category` specifies an expense caregory to look for.

Returns a list of car's replaced parts. Requires authentication.

Response body example:
```JSON
[
    {
        "id": 6,
        "date": "2021-07-23",
        "sum": 5500.0,
        "currency": {
            "id": 1,
            "name": "Ukranian Hryvna",
            "code": "UAH"
        },
        "description": "Dismanting window",
        "categoryDto": {
            "id": 2,
            "name": "REPAIR"
        },
        "carInfoDto": {
            "id": 7,
            "mark": "BMW",
            "model": "M5",
            "color": "white",
            "vin_code": "A1221Z"
        }
    },
    {
        "id": 7,
        "date": "2020-09-23",
        "sum": 300.0,
        "currency": {
            "id": 2,
            "name": "United States Dollar",
            "code": "USD"
        },
        "description": "Dismanting wheels",
        "categoryDto": {
            "id": 2,
            "name": "REPAIR"
        },
        "carInfoDto": {
            "id": 7,
            "mark": "BMW",
            "model": "M5",
            "color": "white",
            "vin_code": "A1221Z"
        }
    }
]
```

### Getting car models rating and it`s reliability details:
`GET /cars/rating`


Returns a map of car model`s reliability info with average crashes amount and average repair cost. Requires authentication and role ADMIN.

Response body example:
```JSON
{
    "Audi": {
        "crashesPerYear": 1,
        "crashesPerMonth": 1,
        "repairCostInYear": 1350.0,
        "repairCostInMonth": 1350.0
    },
    "BMW": {
        "crashesPerYear": 1,
        "crashesPerMonth": 1,
        "repairCostInYear": 2900.0,
        "repairCostInMonth": 2900.0
    }
}
```

### Getting repaired details statistics by car mark:
`GET /repaired_details/statistics`

Query parameters example:

`?mark=BMW`

where `mark` specifies a car`s mark by which would be created statistics.

Returns a list of repaired detail statistics by mark. Requires authentication and role ADMIN.

Response body example:
```JSON
[
    {
        "name": "Wheels",
        "amount": 1,
        "averageCost": 300.0
    },
    {
        "name": "Windows",
        "amount": 1,
        "averageCost": 5500.0
    }
]
```
